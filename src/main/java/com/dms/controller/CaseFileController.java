package com.dms.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.dms.model.ActDetails;
import com.dms.model.ActionResponse;
import com.dms.model.CSVUpload;
import com.dms.model.CSVUploadStage;
import com.dms.model.CSVUtils;
import com.dms.model.CaseCheckListMapping;
import com.dms.model.CaseFileDetail;
import com.dms.model.CaseFileStage;
import com.dms.model.CaseType;
import com.dms.model.Caveat;
import com.dms.model.CaveatOld;
import com.dms.model.CourtFee;
import com.dms.model.District;
import com.dms.model.ImpugnedOrder;
import com.dms.model.IndexField;
import com.dms.model.Lookup;
import com.dms.model.LowerCourtCaseType;
import com.dms.model.PetitionUploaded;
import com.dms.model.PetitionerDetails;
import com.dms.model.RegisteredCaseDetails;
import com.dms.model.RegistrationFileStage;
import com.dms.model.RespondentDetails;
import com.dms.model.Search;
import com.dms.model.StampReporterData;
import com.dms.model.State;
import com.dms.model.TrialCourt;
import com.dms.model.User;
import com.dms.service.CSVUploadService;
import com.dms.service.CaseFileService;
import com.dms.service.CaseFileStageService;
import com.dms.service.LookupService;
import com.dms.utility.GlobalFunction;
import com.lowagie.text.DocumentException;

@Controller
@RequestMapping("/casefile")
public class CaseFileController {
	
	@Autowired
	ServletContext context;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired
	private CSVUploadService CSVUploadService;
	
	@Autowired
	private CaseFileService caseFileService;
	
	@Autowired
	private CaseFileStageService caseFileStageService;
	
	private GlobalFunction globalfunction;	
	
	private Integer uploadCount=0;
	
	public CaseFileController() {
		// registrationPartyValidation = new RegistrationpartyValidation();
		globalfunction = new GlobalFunction();
	}
	@RequestMapping(value = "/manage", method = RequestMethod.GET)
	public String Manage() {

		return "/casefile/manage";
	}
	@RequestMapping(value = "/getCaseFileList", method = RequestMethod.POST)
	public @ResponseBody String getCaseList(@RequestBody Search search,HttpSession session) {
		String jsonData = null;
		System.out.println("StageId="+search.getStageId());
		Long stageId=search.getStageId();
		String diary_no="";
		diary_no=search.getDiary_no();
		String stageDate=null;
		if(search.getStageDate()!=null)
		{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			stageDate = formatter.format(search.getStageDate());
		}
		
		User user=(User) session.getAttribute("USER");

		List<RegisteredCaseDetails> newDraftList = new ArrayList<RegisteredCaseDetails>();
		//Lookup lkreporting=lookupService.getLookup("ECOURT_STAGE", "REPORTING_DONE");
		List<RegisteredCaseDetails> draftDetails=new ArrayList<RegisteredCaseDetails>();
		
		if(diary_no!=null){
			RegisteredCaseDetails r=caseFileService.getByDiaryNo(diary_no);
			draftDetails.add(r);
		}else{
			draftDetails=caseFileService.searchCaseFiles(stageId,stageDate);
		}
		ActionResponse<RegisteredCaseDetails> response = new ActionResponse<RegisteredCaseDetails>();

		PetitionerDetails pDetails =null;

		RespondentDetails rDetails = null;		

		for (RegisteredCaseDetails rcd : draftDetails) {
			pDetails= new PetitionerDetails();
			rDetails= new RespondentDetails();
			pDetails =caseFileService.getFirstPetitioner(rcd.getRcd_id());
			rDetails=caseFileService.getFirstRespondent(rcd.getRcd_id());
			        if(pDetails.getPt_id()!=null){
			         rcd.setPetitionerDetails(pDetails);
			        }
			        if(rDetails.getRt_id()!=null) {
			          rcd.setRespondentDetails(rDetails);
			        }		
			newDraftList.add(rcd);
		}


		if (newDraftList != null && newDraftList.size() != 0){
			response.setResponse("TRUE");
			response.setModelList(newDraftList);
		}else{
			response.setResponse("FALSE");
			}
		
		jsonData = globalfunction.convert_to_json(response);

		return jsonData;
	}
	@RequestMapping(value="/downloadCSV",method=RequestMethod.GET)
	public void downloadCaseFile(HttpServletRequest request,HttpServletResponse response,HttpSession session)
	{
		String date=request.getParameter("date");
		String case_no_generated=request.getParameter("case_no_generated");
		Lookup lkreporting=lookupService.getLookup("ECOURT_STAGE","REPORTING_DONE");	
			List<RegisteredCaseDetails> caseLists=caseFileService.searchCaseFilesForCSV(lkreporting.getLk_id());
		
			Lookup lookUp=lookupService.getLookUpObject("CSV_DOWNLOAD_PATH");
			String zip_download_path=lookUp.getLk_longname();
			Date date1 = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String folder = "cases_"+formatter.format(date1);
			String dest_folder=zip_download_path+File.separator+folder;
			
			File destFolder = new File(dest_folder);
			
			if(!destFolder.exists())
			{
				destFolder.mkdir();
			}
			
			generateCasefiles(dest_folder,caseLists,lkreporting);
			generatePetitioners(dest_folder,caseLists);
			generateRespondents(dest_folder,caseLists);
			generateActDetails(dest_folder,caseLists);
			generateImpungedDetails(dest_folder,caseLists);
			generateCourtFees(dest_folder,caseLists);
			 try 
				{
		        	zipFolder(destFolder.getAbsolutePath(),destFolder.getAbsolutePath()+".zip");
				} 
				catch (Exception e) {
				    e.printStackTrace();
				}
		        
		        // to download file
		        try
		        {
		        response.setContentType("application/zip");  
		        PrintWriter out = response.getWriter();  
		        String filename = destFolder.getName()+".zip";   
		        String filepath = destFolder.getAbsolutePath()+".zip";  
		        response.setContentType("APPLICATION/OCTET-STREAM");   
		        response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
		          
		        FileInputStream fileInputStream = new FileInputStream(filepath);  
		                    
		        int i;   
		        while ((i=fileInputStream.read()) != -1) {  
		        out.write(i);   
		        }   
		        fileInputStream.close();   
		        out.close(); 
		        }catch(Exception e)
		        {
		        	e.printStackTrace();
		        }
			
	    }
	private void generateCourtFees(String dest_folder, List<RegisteredCaseDetails> caseLists) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"courtfees.csv";
		try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "ReceiptNo", "Amount","Date"));

			for (RegisteredCaseDetails r : caseLists) {
				
				List<CourtFee> fees=caseFileService.getCourtFee(r.getRcd_id());
				for (CourtFee cf : fees) {
					
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					String courtFeeDate = formatter.format(cf.getCf_date());
					List<String> list = new ArrayList<>();
					list.add(r.getRcd_draft_no());
				    list.add(r.getRcd_diary_no());
				    list.add(cf.getCf_receipt_no());
				    list.add(cf.getCf_amount().toString());
				    list.add(courtFeeDate);
				    CSVUtils.writeLine(writer, list);
				}
				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void generateImpungedDetails(String dest_folder, List<RegisteredCaseDetails> caseLists) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"lowercourts.csv";
		try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "CourtType", "CaseType","CaseNo","CaseYear","State","District","LowerCourtType","JudgeName","DecisionDate"));

			for (RegisteredCaseDetails r : caseLists) {
				
				List<ImpugnedOrder> lowercourts=caseFileService.getImpugnedOrder(r.getRcd_id());
				for (ImpugnedOrder io : lowercourts) {
					
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					String decisonDate = formatter.format(io.getIo_decision_date());
					List<String> list = new ArrayList<>();
					list.add(r.getRcd_draft_no());
				    list.add(r.getRcd_diary_no());
				    if(io.getIo_court_type()==1)
				    	list.add("LC");
				    else
				    	list.add("HC");
				    if(io.getIo_court_type()==1){
				    	LowerCourtCaseType lct=caseFileService.getLCCaseTypeById(io.getIo_lc_case_type());
				    	list.add(lct.getCt_name());
				    }
				    else{				    	
				    	CaseType ct=caseFileService.getCaseTypeById(io.getIo_hc_case_type());
				    	list.add(ct.getCt_label());
				    }
				    list.add(io.getIo_case_no());
				    list.add(String.valueOf(io.getIo_case_year()));
				    if(io.getIo_s_id()!=null){
				    	State state=caseFileService.getStateById(io.getIo_s_id());
				    	list.add(state.getS_name());
				    }else{
				    	list.add("");
				    }
				    list.add(String.valueOf(io.getIo_district()));
				    list.add(io.getCourtType().getLct_name());
				    list.add(io.getIo_judge_name());
				    list.add(decisonDate);
				    CSVUtils.writeLine(writer, list);
				}
				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void generateActDetails(String dest_folder, List<RegisteredCaseDetails> caseLists) {
		// TODO Auto-generated method stub
		/*String csvFile = dest_folder+File.separator+"acts.csv";
		try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DiaryNo", "Act Title", "Act Rule","Section","Rule No"));

			for (RegisteredCaseDetails r : caseLists) {
				
				List<ActDetails> acts=caseFileService.getActDetails(r.getRcd_id());
				for (ActDetails a : acts) {
					
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					String courtFeeDate = formatter.format(cf.getCf_date());
					List<String> list = new ArrayList<>();
				    list.add(r.getRcd_diary_no());
				    list.add(cf.getCf_receipt_no());
				    list.add(cf.getCf_amount().toString());
				    list.add(courtFeeDate);
				    CSVUtils.writeLine(writer, list);
				}
				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	
	}
	private void generateRespondents(String dest_folder, List<RegisteredCaseDetails> caseLists) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"respondents.csv";
		
        try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "Name", "Email","Address","Mobile","Pincode","City","Other Contacts"));

			for (RegisteredCaseDetails r : caseLists) {
				List<RespondentDetails> respondents=caseFileService.getRespondent(r.getRcd_id());
				int i=0;
				for (RespondentDetails rd : respondents) {
					if(i>0)
					{
						List<String> list = new ArrayList<>();
						list.add(r.getRcd_draft_no());
						list.add(r.getRcd_diary_no());
						list.add(rd.getRt_name());
						if(rd.getRt_email()!=null)
				    		list.add(rd.getRt_email().toString());
				    	else
				    		list.add("");
				    	if(rd.getRt_address()!=null)
				    		list.add(rd.getRt_address().toString());
				    	else
				    		list.add("");
				    	if(rd.getRt_mobile()!=null)
				    		list.add(rd.getRt_mobile().toString());
				    	else
				    		list.add("");
				    	
				    	if(rd.getRt_pincode()!=null)
				    		list.add(rd.getRt_pincode().toString());
				    	else
				    		list.add("");
				    	if(rd.getRt_city()!=null)
				    		list.add(rd.getRt_city().toString());
				    	else
				    		list.add("");
				    	if(rd.getRt_other_contact()!=null)
				    		list.add(rd.getRt_other_contact().toString());
				    	else
				    		list.add("");
				    	CSVUtils.writeLine(writer, list);
					
					}
					i++;
				}
				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void generatePetitioners(String dest_folder, List<RegisteredCaseDetails> caseLists) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"petitioners.csv";
		
        try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "Name", "Email","Address","Mobile","Pincode","City","Other Contacts"));
			
			for (RegisteredCaseDetails r : caseLists) {
				int i=0;
				List<PetitionerDetails> petitioners=caseFileService.getPetitioner(r.getRcd_id());
				for (PetitionerDetails p : petitioners) {
					if(i>0)
					{
						List<String> list = new ArrayList<>();
						list.add(r.getRcd_draft_no());
						list.add(r.getRcd_diary_no());
						list.add(p.getPt_name());
				    	if(p.getPt_email()!=null)
				    		list.add(p.getPt_email().toString());
				    	else
				    		list.add("");
				    	if(p.getPt_address()!=null)
				    		list.add(p.getPt_address().toString());
				    	else
				    		list.add("");
				    	if(p.getPt_mobile()!=null)
				    		list.add(p.getPt_mobile().toString());
				    	else
				    		list.add("");
				    	
				    	if(p.getPt_pincode()!=null)
				    		list.add(p.getPt_pincode().toString());
				    	else
				    		list.add("");
				    	if(p.getPt_city()!=null)
				    		list.add(p.getPt_city().toString());
				    	else
				    		list.add("");
				    	if(p.getPt_other_contact()!=null)
				    		list.add(p.getPt_other_contact().toString());
				    	else
				    		list.add("");
				    	CSVUtils.writeLine(writer, list);
					}
					i++;
				}
				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void generateCasefiles(String dest_folder, List<RegisteredCaseDetails> caseLists,Lookup lkreporting) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"casefiles.csv";
		Lookup lkdraftSubmit=lookupService.getLookup("ECOURT_STAGE","REPORTING_DONE");
        try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "CaseType", "District","Fullname","Username","DOF","Senior citizen","SC/ST","Woman/Child","Divyang","Legal Ald Case","In Custody","Application","Application1","Application2","Application3","Application4","Application5","Cognizable","Category","Defective","PetitionerName","RespondentName"));

			for (RegisteredCaseDetails r : caseLists) {
				StampReporterData srd=caseFileService.getStampReporterData(r.getRcd_id());
				PetitionerDetails pd=caseFileService.getFirstPetitioner(r.getRcd_id());
				RespondentDetails rd=caseFileService.getFirstRespondent(r.getRcd_id());
				
				CaseFileStage stage=caseFileStageService.getByStage(r.getRcd_id(),lkdraftSubmit.getLk_id());
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				String createdDate = formatter.format(stage.getRcs_cr_date());
			    List<String> list = new ArrayList<>();
			    list.add(r.getRcd_draft_no());
			    list.add(r.getRcd_diary_no());			    
			    list.add(r.getCaseType().getCt_label().toString());
			    list.add(r.getRcd_dt_id().toString());
			    list.add(r.getUserMaster().getUm_fullname());
			    if(r.getUserMaster().getUserroles().get(0).getLk().getLk_longname().equals("Advocate"))
			    	list.add(r.getUserMaster().getUsername());
			    else
			    	list.add("");
			    list.add(createdDate.toString());
			    list.add(String.valueOf(r.isRcd_sen_ctz()));
			    list.add(String.valueOf(r.isRcd_sc_st()));
			    list.add(String.valueOf(r.isRcd_wm_ch()));
			    list.add(String.valueOf(r.isRcd_dvg()));
			    list.add(String.valueOf(r.isRcd_lg_ad()));
			    list.add(String.valueOf(r.isRcd_cust()));
			    list.add(srd.getSrd_application());
			    list.add(String.valueOf(srd.getSrd_app_type1()));
			    list.add(String.valueOf(srd.getSrd_app_type2()));
			    list.add(String.valueOf(srd.getSrd_app_type3()));
			    list.add(String.valueOf(srd.getSrd_app_type4()));
			    list.add(String.valueOf(srd.getSrd_app_type5()));
			    list.add(srd.getSrd_cognizable());
			    list.add(srd.getSrd_category());
			    list.add(String.valueOf(srd.getSrd_defective()));
			    list.add(pd.getPt_name());
			    list.add(rd.getRt_name());
			    CSVUtils.writeLine(writer, list);

				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void zipFolder(String srcFolderPath, String zipPath) throws Exception 
    {
    	 File zipFile=new File(zipPath);
    	 final Path sourceFolderPath=Paths.get(srcFolderPath);
        final ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile));
        Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>()
        		{
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                zos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));
                Files.copy(file, zos);
                zos.closeEntry();
                return FileVisitResult.CONTINUE;
            }
        });
        zos.close();
    }
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody String create(MultipartHttpServletRequest request,HttpSession session) throws DocumentException 
	{
		ActionResponse<RegisteredCaseDetails> response = new ActionResponse();
		User u=(User) session.getAttribute("USER");
		String jsonData="";
		this.uploadCount=0;
		Lookup lookup=lookupService.getLookUpObject("CSV_UPLOAD_PATH");
		MultipartFile mpf = null;
    	Iterator<String> itr = request.getFileNames();
    	String csvPath="";
    	Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String currentDate = formatter.format(date);
    	while (itr.hasNext()) 
		{
			mpf = request.getFile(itr.next());
			CSVUpload csvupload=new CSVUpload();
			csvupload.setCu_cr_by(u.getUm_id());
			csvupload.setCu_cr_date(date);
			csvupload.setCu_name(mpf.getName());
			csvupload.setCu_type("F");
			csvupload=CSVUploadService.save(csvupload);
			
			String filename="case"+"_"+csvupload.getCu_id()+".csv";
			csvupload.setCu_name(filename);
			csvupload=CSVUploadService.save(csvupload);
			
			csvPath=lookup.getLk_longname() + File.separator +filename;
			try {
				FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(csvPath));
				updateCaseNo(csvPath,csvupload, u);
			}catch (IOException e) {
				e.printStackTrace();
			}
			
		}
    	String message=uploadCount+" no of files has been updated";
    	
    	response.setResponse("TRUE");
    	response.setData(message);
    	jsonData = globalfunction.convert_to_json(response);
		return jsonData;
	}
	private void updateCaseNo(String csvPath,CSVUpload csvupload,User u) {
		 String csvFile = csvPath;
	        BufferedReader br = null;
	        String line = "";
	        String cvsSplitBy = ",";
	        Lookup lkcaseGenerated=lookupService.getLookup("ECOURT_STAGE", "CASE_NO_GENERATED");	
	        try {

	            br = new BufferedReader(new FileReader(csvFile));
	            while ((line = br.readLine()) != null) {

	                // use comma as separator
	                String[] casefiles = line.split(cvsSplitBy);
	                //DraftNo,DiaryNo,CaseType,CaseNo,CaseYear
	                System.out.println("DiaryNo = " + casefiles[1] + " , caseno=" + casefiles[3] + "]");
	                CaseType caseType=caseFileService.getCaseTypeByLabel(casefiles[2]);
	                
	                CaseFileDetail existCaseFileDetail=caseFileService.getCaseFile(caseType.getCt_id(),casefiles[3],Integer.parseInt(casefiles[4]));
	                if(existCaseFileDetail==null)
	                {
		                RegisteredCaseDetails r=caseFileService.getByDiaryNo(casefiles[1]);
		                PetitionerDetails pd=caseFileService.getFirstPetitioner(r.getRcd_id());
						RespondentDetails rd=caseFileService.getFirstRespondent(r.getRcd_id());
						
		                
		                r.setRcd_ct_id(caseType.getCt_id());
		                r.setRcd_case_no(Integer.parseInt(casefiles[3]));
		                r.setRcd_case_year(Integer.parseInt(casefiles[4]));
		                r.setRcd_stage_lid(lkcaseGenerated.getLk_id());
		                caseFileService.saveCaseDetails(r);
		               
		                CaseFileDetail cfd=new CaseFileDetail();
		                cfd.setFd_case_no(casefiles[3]);
		                cfd.setFd_case_year(Integer.parseInt(casefiles[4]));
		                cfd.setFd_case_type(caseType.getCt_id());
		                cfd.setFd_first_petitioner(pd.getPt_name());
		                cfd.setFd_first_respondent(rd.getRt_name());
		                cfd.setFd_cr_date(new Date());
		                cfd.setFd_rec_status(1);
		                caseFileService.saveCaseFileDetails(cfd);
		                
		                
		                CaseFileStage cfs=new CaseFileStage();
		                cfs.setRcs_cr_by(u.getUm_id());
		                cfs.setRcs_cr_date(new Date());
		                cfs.setRcs_rcd_mid(r.getRcd_id());
		                cfs.setRcs_stage_lid(lkcaseGenerated.getLk_id());
		                caseFileStageService.save(cfs);
		                
		                CSVUploadStage csvstage=new CSVUploadStage();
		                csvstage.setCus_cu_mid(csvupload.getCu_id());
		                csvstage.setCus_file_id(r.getRcd_id());
		                CSVUploadService.save(csvstage);
		                uploadCount++;
	                }
	            }

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	}
	@RequestMapping(value = "/getCaseDetail", method = RequestMethod.GET)
	@ResponseBody
	public String getCaseDetail(HttpServletRequest request) {

		String id = request.getParameter("docId");
		RegisteredCaseDetails response = null;

		Long doc = new Long(id);
		ActionResponse<RegisteredCaseDetails> pd = new ActionResponse<RegisteredCaseDetails>();
		String jsonData = null;
		PetitionerDetails pDetails=null;
		RespondentDetails rDetails=null;
		response = caseFileService.getRegisterCase(doc);
        pDetails=caseFileService.getFirstPetitioner(doc);
		rDetails=caseFileService.getFirstRespondent(doc);
		if(pDetails.getPt_id()!=null){
			response.setPetitionerDetails(pDetails);
		}
          if(rDetails.getRt_id()!=null){
        	  response.setRespondentDetails(rDetails);
          }
		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelData(response);
			jsonData = globalfunction.convert_to_json(pd);
		}
		return jsonData;
	}
	
	@RequestMapping(value = "/getPetitioner", method = RequestMethod.GET)
	@ResponseBody
	public String getRegisterCase(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<PetitionerDetails> response = null;

		Long doc = new Long(id);
		ActionResponse<PetitionerDetails> pd = new ActionResponse<PetitionerDetails>();
		String jsonData = null;

		response = caseFileService.getPetitioner(doc);

		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelList(response);
			jsonData = globalfunction.convert_to_json(pd);

		}
		return jsonData;

	}

	@RequestMapping(value = "/getRespondent", method = RequestMethod.GET)
	@ResponseBody
	public String getRespondent(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<RespondentDetails> response = null;

		Long doc = new Long(id);
		ActionResponse<RespondentDetails> pd = new ActionResponse<RespondentDetails>();
		String jsonData = null;

		response = caseFileService.getRespondent(doc);

		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelList(response);
			jsonData = globalfunction.convert_to_json(pd);

		}
		return jsonData;

	}
	
	@RequestMapping(value = "/getActDetails", method = RequestMethod.GET)
	@ResponseBody
	public String getActDetails(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<ActDetails> response = null;

		Long doc = new Long(id);
		ActionResponse<ActDetails> pd = new ActionResponse<ActDetails>();
		String jsonData = null;

		response = caseFileService.getActDetails(doc);

		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelList(response);
			jsonData = globalfunction.convert_to_json(pd);

		}
		return jsonData;

	}
	
	@RequestMapping(value = "/getImpugnedOrder", method = RequestMethod.GET)
	@ResponseBody
	public String getImpugnedOrder(HttpServletRequest request) {
		String jsonData = "";
		String id = request.getParameter("docId");
		Long doc = new Long(id);
		List<ImpugnedOrder> impugnedOrderList = caseFileService.getImpugnedOrder(doc);
		ActionResponse<ImpugnedOrder> response = new ActionResponse<ImpugnedOrder>();
		List<ImpugnedOrder> newImpugnedOrderList = new ArrayList<ImpugnedOrder>();
		for(ImpugnedOrder io:impugnedOrderList){
			ImpugnedOrder temp=new ImpugnedOrder();
			temp=io;
			if(io.getIo_hc_case_type()!=null){
				CaseType ct=caseFileService.getCaseTypeById(temp.getIo_hc_case_type());
				temp.setHcCaseType(ct);
			}
			if(io.getIo_lc_case_type()!=null){
				LowerCourtCaseType lc=caseFileService.getLCCaseTypeById(temp.getIo_lc_case_type());
				temp.setLcCaseType(lc);
			}
			if(io.getIo_district()!=null){
				District dt=caseFileService.getDistrictById(temp.getIo_district());
				temp.setDistrict(dt);
			}
			newImpugnedOrderList.add(temp);
		}
		if (impugnedOrderList != null) {
			response.setResponse("TRUE");
			response.setModelList(newImpugnedOrderList);
			jsonData = globalfunction.convert_to_json(response);

		}
		return jsonData;
	}
	
	
	@RequestMapping(value = "/getTrialCourt", method = RequestMethod.GET)
	@ResponseBody
	public String getTrialCourt(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<TrialCourt> response = null;

		Long doc = new Long(id);
		ActionResponse<TrialCourt> pd = new ActionResponse<TrialCourt>();
		String jsonData = null;

		response = caseFileService.getTrialCourt(doc);

		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelList(response);
			jsonData = globalfunction.convert_to_json(pd);

		}
		return jsonData;

	}
	
	
	@RequestMapping(value = "/getCourtFee", method = RequestMethod.GET)
	@ResponseBody
	public String getCourtFee(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<CourtFee> response = null;

		Long doc = new Long(id);
		ActionResponse<CourtFee> pd = new ActionResponse<CourtFee>();
		String jsonData = null;

		response = caseFileService.getCourtFee(doc);

		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelList(response);
			jsonData = globalfunction.convert_to_json(pd);

		}
		return jsonData;
	}
	@RequestMapping(value="/getUploadedDocuments", method=RequestMethod.GET)
	@ResponseBody
	public String getUploadedDocuments(HttpServletRequest request)
	{
		String jsonData=null;
		String file_id=request.getParameter("rcd_id");
		Long rcd_id=Long.valueOf(file_id);
		
		List<PetitionUploaded> documentList=caseFileService.getUploadedPetition(rcd_id);
		ActionResponse<PetitionUploaded> response=new ActionResponse<PetitionUploaded>();
		
		response.setModelList(documentList);
		response.setResponse("TRUE");
		
	     jsonData=globalfunction.convert_to_json(response);
	     return jsonData;
	}
	
	@RequestMapping(value = "/searchCaveat", method = RequestMethod.GET)
	@ResponseBody
	public String searchCaveat(HttpServletRequest request) {

		String id = request.getParameter("docId");
		RegisteredCaseDetails caseDetail = new RegisteredCaseDetails();

		Long doc = new Long(id);
		ActionResponse<Caveat> response = new ActionResponse<Caveat>();
		String jsonData = null;
		
		caseDetail = caseFileService.getRegisterCase(doc);
		List<ImpugnedOrder> impungedOrders = caseFileService.getImpugnedOrder(doc);
	
		Long ct_type=caseDetail.getRcd_ct_id();
		Long dist_id=caseDetail.getRcd_dt_id();
		String dates="";
		List<Caveat> caveatList = new ArrayList<Caveat>() ;
		
		for(ImpugnedOrder im:impungedOrders){
			if(im.getIo_decision_date()!=null){
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				String date = formatter.format(im.getIo_decision_date());
				date="'"+date+"'";
				dates+=date+",";
			}
		}
		
		if(dates!=""){
			dates=dates.substring(0, dates.length() - 1);
		}
		caveatList=caseFileService.searchCaveat(ct_type,dist_id,dates);
		
		
		List<Caveat> newCaveatList=new ArrayList<Caveat>();
		for(Caveat c :caveatList){
			Caveat temp=new Caveat();
			temp=c;
			if(c.getCav_hc_case_type()!=null){
				CaseType ct=caseFileService.getCaseTypeById(c.getCav_hc_case_type());
				temp.setHcCaseType(ct);
			}
			if(c.getCav_lc_case_type()!=null){
				LowerCourtCaseType ct=caseFileService.getLCCaseTypeById(c.getCav_lc_case_type());
				temp.setLcCaseType(ct);
			}
			newCaveatList.add(temp);
			
		}
		response.setModelList(newCaveatList);
		jsonData=globalfunction.convert_to_json(response);
		return jsonData;

	}
	@RequestMapping(value = "/searchCaveatold", method = RequestMethod.GET)
	@ResponseBody
	public String searchCaveatold(HttpServletRequest request) {

		String id = request.getParameter("docId");
		RegisteredCaseDetails caseDetail = new RegisteredCaseDetails();

		Long doc = new Long(id);
		ActionResponse<CaveatOld> response = new ActionResponse<CaveatOld>();
		String jsonData = null;
		
		caseDetail = caseFileService.getRegisterCase(doc);
		List<ImpugnedOrder> impungedOrders = caseFileService.getImpugnedOrder(doc);
		Long ct_type=caseDetail.getRcd_ct_id();
		Long dist_id=caseDetail.getRcd_dt_id();
		String dates="";
		List<CaveatOld> caveatList = new ArrayList<CaveatOld>() ;
		
		for(ImpugnedOrder im:impungedOrders){
			if(im.getIo_decision_date()!=null){
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				String date = formatter.format(im.getIo_decision_date());
				date="'"+date+"'";
				dates+=date+",";
			}
		}
		
		if(dates!=""){
			dates=dates.substring(0, dates.length() - 1);
		}
		caveatList=caseFileService.searchCaveatOld(ct_type,dist_id,dates);
		
		response.setModelList(caveatList);
		
		List<CaveatOld> newCaveatList=new ArrayList<CaveatOld>();
		/*for(CaveatOld c :caveatList){
			CaveatOld temp=new CaveatOld();
			temp=c;
			if(c.getCav_hc_case_type()!=null){
				CaseType ct=ecourtAddCaseService.getCaseTypeById(c.getCav_hc_case_type());
				temp.setHcCaseType(ct);
			}
			if(c.getCav_lc_case_type()!=null){
				LowerCourtCaseType ct=ecourtAddCaseService.getLCCaseTypeById(c.getCav_lc_case_type());
				temp.setLcCaseType(ct);
			}
			newCaveatList.add(temp);
			
		}*/
		jsonData=globalfunction.convert_to_json(response);
		return jsonData;

	}
	@RequestMapping(value = "/getCaseStages", method = RequestMethod.GET)
	public @ResponseBody String getCaseStages(HttpServletRequest request) {
		String jsonData="";
		String id = request.getParameter("docId");
		Long docId = new Long(id);
		ActionResponse<CaseFileStage> response=new ActionResponse<CaseFileStage>();
		List<CaseFileStage> stages=caseFileStageService.getStages(docId);
		response.setData("TRUE");
		response.setModelList(stages);
		jsonData=globalfunction.convert_to_json(response);
		return jsonData;	
	}
	
	@RequestMapping(value="/copyFile",method=RequestMethod.GET)
	@ResponseBody
	public String copyFiles(HttpServletRequest request)
	{
		String jsonData = null;
		ActionResponse<IndexField> response= new ActionResponse<IndexField>();
		
		String file_id=request.getParameter("rcd_id");
		Long rcd_id=Long.valueOf(file_id);
		
		List<PetitionUploaded> documentList=caseFileService.getUploadedPetition(rcd_id);
		
		if(documentList.size()==1)
		{
			String doc_name=documentList.get(0).getPu_document_name();
			
			Lookup lookUp=lookupService.getLookUpObject("DRAFT_PATH");	
			String draft_path=lookUp.getLk_longname();
	
			File source = new File(draft_path+File.separator+doc_name);	
			String uploadPath = context.getRealPath("");
			
			doc_name="case_"+doc_name;
			File dest = new File(uploadPath+"/uploads/"+doc_name);
	
			try {
				    FileUtils.copyFile(source, dest);
				    response.setResponse("TRUE");
				    response.setData(doc_name);
				} 
				catch (IOException e) {
				    e.printStackTrace();
				    response.setResponse("FALSE");
				    response.setData("Error in displaying file");
				}
		}else
		{
			response.setResponse("FALSE");
			response.setData("File not found");
		}
		jsonData = globalfunction.convert_to_json(response);
		return jsonData;
	}
	
	@RequestMapping(value = "/getReport", method = RequestMethod.GET)
	@ResponseBody
	public String getCaseHistory(HttpServletRequest request) {

		String id = request.getParameter("docId");
		Long doc = new Long(id);
		String jsonData=null,srdData=null,caveatData=null,caveatOldData = null,courtFeesData=null;

		StampReporterData srd = caseFileService.getStampReporterData(doc);
		Caveat caveat=null;
		CaveatOld caveatOld=null;
		Long courtFees=null;
		
		if(srd!=null)
		{
			courtFees=caseFileService.getEcourtFeesSum(doc);
			if(srd.getSrd_cav_no()!=null)
			{
				caveatOld = caseFileService.getCaveatOld(srd.getSrd_cav_no(), srd.getSrd_cav_year());
				if(caveatOld==null)
				{
					caveat = caseFileService.getCaveat(srd.getSrd_cav_no(), srd.getSrd_cav_year());
				}
			}
			
		}
		if(srd!=null){
			srdData=globalfunction.convert_to_json(srd);
		}
		if(caveat!=null){
			caveatData=globalfunction.convert_to_json(caveat);
		}
		if(caveatOld!=null){
			caveatOldData=globalfunction.convert_to_json(caveatOld);
		}
		if(courtFees!=null){
			courtFeesData=globalfunction.convert_to_json(courtFees);
		}
		
		
		if(caveatOld==null && caveat==null)
		{
			jsonData = "{\"srd\":"+srdData+",\"courtFee\":"+courtFeesData+"}";
		}
		else if(caveat==null)
		{
			jsonData = "{\"srd\":"+srdData+",\"courtFee\":"+courtFeesData+",\"caveat\":"+caveatOldData+"}";
		}
		else
		{
			jsonData = "{\"srd\":"+srdData+",\"courtFee\":"+courtFeesData+",\"caveat\":"+caveatData+"}";
		}

		return jsonData;

	}
	
}
