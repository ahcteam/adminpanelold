package com.dms.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.postgresql.jdbc2.ArrayAssistantRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.dms.model.ActionResponse;
import com.dms.model.Application;
import com.dms.model.ApplicationCourtFee;
import com.dms.model.ApplicationStage;
import com.dms.model.ApplicationTypes;
import com.dms.model.ApplicationUploaded;
import com.dms.model.CSVUpload;
import com.dms.model.CSVUploadStage;
import com.dms.model.CSVUtils;
import com.dms.model.IndexField;
import com.dms.model.Lookup;
import com.dms.model.PetitionUploaded;
import com.dms.model.RegisteredCaseDetails;
import com.dms.model.Search;
import com.dms.model.User;
import com.dms.service.ApplicationService;
import com.dms.service.CSVUploadService;
import com.dms.service.CaseFileStageService;
import com.dms.service.LookupService;
import com.dms.service.UserRoleService;
import com.dms.utility.GlobalFunction;
import com.lowagie.text.DocumentException;

@Controller
@RequestMapping("/application")
public class ApplicationController 
{
	private GlobalFunction globalfunction;
	@Autowired
	ServletContext context;
	@Autowired
	private ApplicationService applicationService;
	
	
	@Autowired
	private CSVUploadService CSVUploadService;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired
	private UserRoleService userRoleService;
	
	@Autowired
	private CaseFileStageService caseFileStageService;
	
	private Integer uploadCount=0;
	public ApplicationController()
	{
		globalfunction = new GlobalFunction();
	}
	
	@RequestMapping(value = "/manage", method = RequestMethod.GET)
	public String Manage() {
		return "/application/manage";
	}
	@RequestMapping(value = "/getApplicationList", method = RequestMethod.POST)
	public @ResponseBody String getCaseList(@RequestBody Search search,HttpSession session) {
		String jsonData = null;
		
		User user=(User) session.getAttribute("USER");
		Long stageId=search.getStageId();
		String diary_no="";
		diary_no=search.getDiary_no();
		String stageDate=null;
		if(search.getStageDate()!=null)
		{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			stageDate = formatter.format(search.getStageDate());
		}
		List<Application> newDraftList = new ArrayList<Application>();
		Lookup lkreporting=lookupService.getLookup("ECOURT_STAGE", "REPORTING_DONE");
		
		List<Application> applications=new ArrayList<Application>();
		
		if(diary_no!=null){
			Application a=applicationService.getByDiaryNo(diary_no);
			applications.add(a);
		}
		else{
			applications=applicationService.searchApplicationFiles(stageId,stageDate);
		}
		ActionResponse<Application> response = new ActionResponse<Application>();

		
		if (applications != null && applications.size() != 0){
			response.setResponse("TRUE");
			response.setModelList(applications);
		}else{
			response.setResponse("FALSE");
			}
		
		jsonData = globalfunction.convert_to_json(response);

		return jsonData;
	}
	@RequestMapping(value="/downloadCSV",method=RequestMethod.GET)
	public void downloadCaseFile(HttpServletRequest request,HttpServletResponse response,HttpSession session)
	{
		String date=request.getParameter("date");
		String case_no_generated=request.getParameter("case_no_generated");
		Lookup lkreporting=lookupService.getLookup("ECOURT_STAGE","REPORTING_DONE");
			List<Application> caseLists=applicationService.searchApplicationFilesForCSV(lkreporting.getLk_id());
		
			Lookup lookUp=lookupService.getLookUpObject("CSV_DOWNLOAD_PATH");
			String zip_download_path=lookUp.getLk_longname();
			Date date1 = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String folder = "applications_"+formatter.format(date1);
			String dest_folder=zip_download_path+File.separator+folder;
			
			File destFolder = new File(dest_folder);
			
			if(!destFolder.exists())
			{
				destFolder.mkdir();
			}
			
			generateApplications(dest_folder,caseLists,lkreporting);
			generateCourtFees(dest_folder,caseLists);
			 try 
				{
		        	zipFolder(destFolder.getAbsolutePath(),destFolder.getAbsolutePath()+".zip");
				} 
				catch (Exception e) {
				    e.printStackTrace();
				}
		        
		        // to download file
		        try
		        {
		        response.setContentType("application/zip");  
		        PrintWriter out = response.getWriter();  
		        String filename = destFolder.getName()+".zip";   
		        String filepath = destFolder.getAbsolutePath()+".zip";  
		        response.setContentType("APPLICATION/OCTET-STREAM");   
		        response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
		          
		        FileInputStream fileInputStream = new FileInputStream(filepath);  
		                    
		        int i;   
		        while ((i=fileInputStream.read()) != -1) {  
		        out.write(i);   
		        }   
		        fileInputStream.close();   
		        out.close(); 
		        }catch(Exception e)
		        {
		        	e.printStackTrace();
		        }
			
	    }
	private void generateCourtFees(String dest_folder, List<Application> applications) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"courtfees.csv";
		try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "ReceiptNo", "Amount","Date"));

			for (Application a : applications) {
				
				List<ApplicationCourtFee> fees=applicationService.getCourtFee(a.getAp_id());
				for (ApplicationCourtFee cf : fees) {
					
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					String courtFeeDate = formatter.format(cf.getAcf_date());
					List<String> list = new ArrayList<>();
					list.add(a.getAp_draft_no());
				    list.add(a.getAp_diary_no());
				    list.add(cf.getAcf_receipt_no());
				    list.add(cf.getAcf_amount().toString());
				    list.add(courtFeeDate);
				    CSVUtils.writeLine(writer, list);
				}
				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void generateApplications(String dest_folder, List<Application> applications,Lookup lkreporting) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"applications.csv";
		
		Lookup lkdraftSubmit=lookupService.getLookup("ECOURT_STAGE","REPORTING_DONE");
		
        try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "CaseType","CaseNo","CaseYear","ApplicationType","ApplicantName","ApplicantEmail","ApplicantMobile","Fullname","Username","DOF"));

			for (Application a : applications) {
				ApplicationStage apStage=applicationService.getApplicationStage(a.getAp_id(),lkdraftSubmit.getLk_id());
				ApplicationTypes applicationType=applicationService.getApplicationType(a.getAp_at_mid());
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				String createdDate = formatter.format(apStage.getAs_cr_date());
			    List<String> list = new ArrayList<>();
			    list.add(a.getAp_draft_no());
			    list.add(a.getAp_diary_no());			    
			    list.add(a.getCaseFileDetail().getCaseType().getCt_label());
			    list.add(String.valueOf(a.getCaseFileDetail().getFd_case_no()));
			    list.add(String.valueOf(a.getCaseFileDetail().getFd_case_year()));
			    list.add(String.valueOf(applicationType.getAt_code()));
			    list.add(a.getAp_applicant_name());
			    list.add(a.getAp_appl_email());
			    list.add(a.getAp_appl_mobile());
			    list.add(a.getUserMaster().getUm_fullname());
			    if(a.getUserMaster().getUserroles().get(0).getLk().getLk_longname().equals("Advocate"))
			    	list.add(a.getUserMaster().getUsername());
			    else
			    	list.add("");
			    list.add(createdDate);
			    CSVUtils.writeLine(writer, list);

				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void zipFolder(String srcFolderPath, String zipPath) throws Exception 
    {
    	 File zipFile=new File(zipPath);
    	 final Path sourceFolderPath=Paths.get(srcFolderPath);
        final ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile));
        Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>()
        		{
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                zos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));
                Files.copy(file, zos);
                zos.closeEntry();
                return FileVisitResult.CONTINUE;
            }
        });
        zos.close();
    }
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody String create(MultipartHttpServletRequest request,HttpSession session) throws DocumentException 
	{
		this.uploadCount=0;
		ActionResponse<RegisteredCaseDetails> response = new ActionResponse();
		User u=(User) session.getAttribute("USER");
		String jsonData="";
		Lookup lookup=lookupService.getLookUpObject("CSV_UPLOAD_PATH");
		MultipartFile mpf = null;
    	Iterator<String> itr = request.getFileNames();
    	String csvPath="";
    	Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = formatter.format(date);
    	while (itr.hasNext()) 
		{
			mpf = request.getFile(itr.next());
			CSVUpload csvupload=new CSVUpload();
			csvupload.setCu_cr_by(u.getUm_id());
			csvupload.setCu_cr_date(date);
			csvupload.setCu_name(mpf.getName());
			csvupload.setCu_type("A");
			csvupload=CSVUploadService.save(csvupload);
			
			String filename="appl"+"_"+csvupload.getCu_id()+".csv";
			csvupload.setCu_name(filename);
			csvupload=CSVUploadService.save(csvupload);
			
			csvPath=lookup.getLk_longname() + File.separator +filename;
			try {
				FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(csvPath));
				updateCaseNo(csvPath,csvupload,u);
			}catch (IOException e) {
				e.printStackTrace();
			}
			
		}
    	String message=uploadCount+" no of files has been updated";
    	
    	response.setResponse("TRUE");
    	response.setData(message);
    	jsonData = globalfunction.convert_to_json(response);
		return jsonData;
	}
	private void updateCaseNo(String csvPath,CSVUpload csvupload,User u) {
		// TODO Auto-generated method stub
		 String csvFile = csvPath;
	        BufferedReader br = null;
	        String line = "";
	        String cvsSplitBy = ",";
	        Lookup lkcaseGenerated=lookupService.getLookup("ECOURT_STAGE", "CASE_NO_GENERATED");
	        try {

	            br = new BufferedReader(new FileReader(csvFile));
	            while ((line = br.readLine()) != null) {

	                // use comma as separator
	                String[] casefiles = line.split(cvsSplitBy);
	                //DraftNo,DiaryNo,ApplicationNo,ApplicationYear
	                System.out.println("DraftNo = " + casefiles[0] + " , applicationno=" + casefiles[2] + "]");
	                Application a=applicationService.getByDiaryNo(casefiles[1]);
	                ApplicationStage astemp=applicationService.getApplicationByStage(a.getAp_id(), 1000043L);
	                if(astemp==null){
	                a.setAp_no(Integer.parseInt(casefiles[2]));
	                a.setAp_year(Integer.parseInt(casefiles[3]));
	                a.setAp_stage_lid(lkcaseGenerated.getLk_id());
	                applicationService.save(a);
	                
	                ApplicationStage as=new ApplicationStage();
	    			as.setAs_ap_mid(a.getAp_id());
	    			as.setAs_stage_lid(lkcaseGenerated.getLk_id());
	    			as.setAs_cr_by(u.getUm_id());
	    			as.setAs_cr_date(new Date());
	    			applicationService.saveApplicationStage(as);
	    			
	                CSVUploadStage csvstage=new CSVUploadStage();
	                csvstage.setCus_cu_mid(csvupload.getCu_id());
	                csvstage.setCus_file_id(a.getAp_id());
	                CSVUploadService.save(csvstage);
	                uploadCount++;
	                }
	            }

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	}
	@RequestMapping(value = "/getRegisterApplication", method = RequestMethod.GET)
	@ResponseBody
	public String getRegisterApplication(HttpServletRequest request) {

		String id = request.getParameter("docId");
		Application cav = null;

		Long doc = new Long(id);
		ActionResponse<Application> response = new ActionResponse<Application>();
		String jsonData = null;

		cav = applicationService.getRegisterApplication(doc);

		if (cav != null) {
			response.setResponse("TRUE");
			response.setModelData(cav);
			jsonData = globalfunction.convert_to_json(response);

		}
		return jsonData;

	}
	@RequestMapping(value = "/getCourtFee", method = RequestMethod.GET)
	@ResponseBody
	public String getCourtFee(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<ApplicationCourtFee> response = null;

		Long doc = new Long(id);
		ActionResponse<ApplicationCourtFee> pd = new ActionResponse<ApplicationCourtFee>();
		String jsonData = null;

		response = applicationService.getApplicationCourtFee(doc);

		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelList(response);
			jsonData = globalfunction.convert_to_json(pd);

		}
		return jsonData;

	}
	
	@RequestMapping(value="/getUploadedDocuments", method=RequestMethod.GET)
	@ResponseBody
	public String getUploadedDocuments(HttpServletRequest request)
	{
		String jsonData=null;
		String file_id=request.getParameter("ap_id");
		Long rcd_id=Long.valueOf(file_id);
		
		List<ApplicationUploaded> documentList=applicationService.getUploadedApplications(rcd_id);
		ActionResponse<ApplicationUploaded> response=new ActionResponse<ApplicationUploaded>();
		
		response.setModelList(documentList);
		response.setResponse("TRUE");
		
	     jsonData=globalfunction.convert_to_json(response);
	     return jsonData;
	}
	@RequestMapping(value = "/getApplicationTypes", method = RequestMethod.GET)
	@ResponseBody
	public String getApplicationTypes(HttpServletRequest request) {

		List<ApplicationTypes> app = null;

		ActionResponse<ApplicationTypes> response = new ActionResponse<ApplicationTypes>();
		String jsonData = null;

		app = applicationService.getApplicationTypes();

		if (app != null) {
			response.setResponse("TRUE");
			response.setModelList(app);
			jsonData = globalfunction.convert_to_json(response);

		}
		return jsonData;

	}
	@RequestMapping(value = "/getApplicationStages", method = RequestMethod.GET)
	public @ResponseBody String getCaseStages(HttpServletRequest request) {
		String jsonData="";
		String id = request.getParameter("docId");
		Long docId = new Long(id);
		ActionResponse<ApplicationStage> response=new ActionResponse<ApplicationStage>();
		List<ApplicationStage> stages=applicationService.getStages(docId);
		response.setData("TRUE");
		response.setModelList(stages);
		jsonData=globalfunction.convert_to_json(response);
		return jsonData;	
	}
	
	@RequestMapping(value="/copyApplicationFile",method=RequestMethod.GET)
	@ResponseBody
	public String copyApplicationFile(HttpServletRequest request)
	{
		String jsonData = null;
		ActionResponse<IndexField> response= new ActionResponse<IndexField>();
		
		String file_id=request.getParameter("ap_id");
		Long ap_id=Long.valueOf(file_id);
		
		List<ApplicationUploaded> documentList=applicationService.getUploadedApplications(ap_id);
		
		if(documentList.size()==1)
		{
			String doc_name=documentList.get(0).getAu_document_name();
			
			Lookup lookUp=lookupService.getLookUpObject("APPLICATION_PATH");	
			String draft_path=lookUp.getLk_longname();
	
			File source = new File(draft_path+File.separator+doc_name);	
			String uploadPath = context.getRealPath("");
			
			doc_name="appl_"+doc_name;
			File dest = new File(uploadPath+"/uploads/"+doc_name);
	
			try {
				    FileUtils.copyFile(source, dest);
				    response.setResponse("TRUE");
				    response.setData(doc_name);
				} 
				catch (IOException e) {
				    e.printStackTrace();
				    response.setResponse("FALSE");
				    response.setData("Error in displaying file");
				}
		}else
		{
			response.setResponse("FALSE");
			response.setData("File not found");
		}
		jsonData = globalfunction.convert_to_json(response);
		return jsonData;
	}

}
