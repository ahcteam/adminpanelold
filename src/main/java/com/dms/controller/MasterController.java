package com.dms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dms.model.ActionResponse;
import com.dms.model.Lookup;
import com.dms.service.LookupService;
import com.dms.utility.GlobalFunction;

@Controller
@RequestMapping("/master")
public class MasterController {

	static int seq = 0;
	private GlobalFunction globalfunction;
	@Autowired
	private LookupService lookupService;
	
	public MasterController() {
		// TODO Auto-generated constructor stub
		globalfunction = new GlobalFunction();
	}

	@RequestMapping(value = "/getstages", method = RequestMethod.GET)
	public @ResponseBody String adminHome() {
		String jsonData="";
		ActionResponse<Lookup> lookup=new ActionResponse<>();
		List<Lookup> lkstages=lookupService.getAll("ECOURT_STAGE");
		lookup.setModelList(lkstages);
		
		jsonData=globalfunction.convert_to_json(lookup);
		
		return jsonData;

	}
}
