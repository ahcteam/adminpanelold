package com.dms.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dms.model.ActionResponse;
import com.dms.model.Application;
import com.dms.model.ApplicationCheckListMapping;
import com.dms.model.ApplicationStage;
import com.dms.model.CaseCheckListMapping;
import com.dms.model.CaseFileStage;
import com.dms.model.Caveat;
import com.dms.model.CaveatCheckListMapping;
import com.dms.model.CaveatFileStage;
import com.dms.model.CaveatPetitionerDetails;
import com.dms.model.CaveatRespondentDetails;
import com.dms.model.CaveatStage;
import com.dms.model.CheckList;
import com.dms.model.IndexField;
import com.dms.model.Lookup;
import com.dms.model.PetitionerDetails;
import com.dms.model.RegisteredCaseDetails;
import com.dms.model.RespondentDetails;
import com.dms.model.StampReporterData;
import com.dms.model.User;
import com.dms.service.ApplicationService;
import com.dms.service.CaseFileService;
import com.dms.service.CaseFileStageService;
import com.dms.service.CaveatService;
import com.dms.service.EcourtAddCaseService;
import com.dms.service.EcourtHomeService;
import com.dms.service.LookupService;
import com.dms.service.ScrutinyService;
import com.dms.utility.GlobalFunction;

@Controller
@RequestMapping("/supervisor")
public class SupervisorController 
{
	private GlobalFunction cm;
	
	@Autowired
	ServletContext context;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired
	private ScrutinyService scrutinyService;
	
	@Autowired
	private CaveatService caveatService;
	@Autowired
	private CaseFileService caseFileService;
	
	@Autowired
	private ApplicationService applicationService;
	
	@Autowired
	private CaseFileStageService caseFileStageService;
	
	public SupervisorController()
	{
		cm = new GlobalFunction();
	}
	@RequestMapping(value = "/submit_case_file", method = RequestMethod.POST)
	public @ResponseBody String submitCaseFile(@RequestBody RegisteredCaseDetails rcdetails,HttpSession session)
	{
		User u = (User) session.getAttribute("USER");

		String jsonData = null;
		ActionResponse<RegisteredCaseDetails> response = new ActionResponse();
		
		RegisteredCaseDetails rcd=caseFileService.getRegisterCase(rcdetails.getRcd_id());
		
		if(rcd.getRcd_stage_lid().longValue()==1000037L){
			// if rejected by scrutiny user
			if(rcdetails.getStatus().equals("Approve")){
				rcd.setRcd_stage_lid(1000041L);
			}
			if(rcdetails.getStatus().equals("Reject")){
				rcd.setRcd_stage_lid(1000039L);
			}
		}
		if(rcd.getRcd_stage_lid().longValue()==1000038){
			// if approved by scrutiny user
			if(rcdetails.getStatus().equals("Reject")){
				rcd.setRcd_stage_lid(1000040L);
			}
			if(rcdetails.getStatus().equals("Approve")){
				rcd.setRcd_stage_lid(1000042L);
			}
		}
		rcd.setRcd_assign_to(null);
		rcd=caseFileService.saveCaseDetails(rcd);
		
		CaseFileStage cfs=new CaseFileStage();
		cfs.setRcs_rcd_mid(rcd.getRcd_id());
		cfs.setRcs_stage_lid(rcd.getRcd_stage_lid());
		cfs.setRcs_cr_by(u.getUm_id());
		cfs.setRcs_cr_date(new Date());
		
		caseFileStageService.save(cfs);
		
		response.setResponse("SUBMIT");
		
		jsonData=cm.convert_to_json(response);

		return jsonData;
	}
	@RequestMapping(value = "/submit_application_file", method = RequestMethod.POST,consumes = {"application/xml", "application/json"})
	public @ResponseBody String submitApplicationFile(@RequestBody Application application,HttpSession session)
	 {
		User u = (User) session.getAttribute("USER");

		String jsonData = null;
		ActionResponse<Caveat> response = new ActionResponse();
		
		Application app=applicationService.getByPk(application.getAp_id());
		if(app.getAp_stage_lid().longValue()==1000037L){
			// if rejected by scrutiny user
			if(application.getStatus().equals("Approve")){
				app.setAp_stage_lid(1000041L);
			}
			if(application.getStatus().equals("Reject")){
				app.setAp_stage_lid(1000039L);
			}
		}
		if(app.getAp_stage_lid().longValue()==1000038){
			// if approved by scrutiny user
			if(application.getStatus().equals("Reject")){
				app.setAp_stage_lid(1000040L);
			}
			if(application.getStatus().equals("Approve")){
				app.setAp_stage_lid(1000042L);
			}
		}
		//app.setAp_stage_lid(application.getAp_stage_lid());//scrutiny done
		app.setAp_assign_to(null);	
		app=applicationService.save(app);
		
		ApplicationStage cfs=new ApplicationStage();
		
		cfs.setAs_ap_mid(app.getAp_id());
		cfs.setAs_stage_lid(app.getAp_stage_lid());
		cfs.setAs_cr_by(u.getUm_id());
		cfs.setAs_cr_date(new Date());
		
		caseFileStageService.saveApplication(cfs);
		
		response.setResponse("SUBMIT");
		jsonData=cm.convert_to_json(response);

		return jsonData;
	 }
	@RequestMapping(value = "/submit_caveat_file", method = RequestMethod.POST,consumes = {"application/xml", "application/json"})
	public @ResponseBody String submitCaveatFile(@RequestBody Caveat cavDetails,HttpSession session)
	 {
		User u = (User) session.getAttribute("USER");

		String jsonData = null;
		ActionResponse<Caveat> response = new ActionResponse();
		
		Caveat cav=caveatService.getByPk(cavDetails.getCav_id());
		if(cav.getCav_stage_lid().longValue()==1000037L){
			// if rejected by scrutiny user
			if(cavDetails.getStatus().equals("Approve")){
				cav.setCav_stage_lid(1000041L);
			}
			if(cavDetails.getStatus().equals("Reject")){
				cav.setCav_stage_lid(1000039L);
			}
		}
		if(cav.getCav_stage_lid().longValue()==1000038){
			// if approved by scrutiny user
			if(cavDetails.getStatus().equals("Reject")){
				cav.setCav_stage_lid(1000040L);
			}
			if(cavDetails.getStatus().equals("Approve")){
				cav.setCav_stage_lid(1000042L);
			}
		}
		//cav.setCav_stage_lid(cavDetails.getCav_stage_lid());//scrutiny done
		cav.setCav_assign_to(null);	
		cav=caveatService.save(cav);
		
		CaveatStage cs=new CaveatStage();
		cs.setCs_cav_mid(cav.getCav_id());
		cs.setCs_stage_lid(cav.getCav_stage_lid());
		cs.setCs_cr_by(u.getUm_id());
		cs.setCs_cr_date(new Date());
		
		caveatService.saveCaveatStage(cs);
		
		response.setResponse("SUBMIT");
		
		  
		jsonData=cm.convert_to_json(response);

		return jsonData;
	 }


}

