package com.dms.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dms.model.ActionResponse;
import com.dms.model.Application;
import com.dms.model.ApplicationCheckListMapping;
import com.dms.model.ApplicationStage;
import com.dms.model.CaseCheckListMapping;
import com.dms.model.CaseFileStage;
import com.dms.model.CaseType;
import com.dms.model.Caveat;
import com.dms.model.CaveatCheckListMapping;
import com.dms.model.CaveatFileStage;
import com.dms.model.CaveatPetitionerDetails;
import com.dms.model.CaveatRespondentDetails;
import com.dms.model.CaveatStage;
import com.dms.model.CheckList;
import com.dms.model.IndexField;
import com.dms.model.Lookup;
import com.dms.model.PetitionerDetails;
import com.dms.model.RegisteredCaseDetails;
import com.dms.model.RespondentDetails;
import com.dms.model.StampReporterData;
import com.dms.model.User;
import com.dms.service.ApplicationService;
import com.dms.service.CaseFileService;
import com.dms.service.CaseFileStageService;
import com.dms.service.CaveatService;
import com.dms.service.EcourtAddCaseService;
import com.dms.service.EcourtHomeService;
import com.dms.service.LookupService;
import com.dms.service.ScrutinyService;
import com.dms.utility.GlobalFunction;

@Controller
@RequestMapping("/scrutiny")
public class ScrutinyController 
{
	private GlobalFunction cm;
	
	@Autowired
	ServletContext context;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired
	private ScrutinyService scrutinyService;
	
	@Autowired
	private CaveatService caveatService;
	@Autowired
	private CaseFileService caseFileService;
	
	@Autowired
	private ApplicationService applicationService;
	
	@Autowired
	private CaseFileStageService caseFileStageService;
	
	public ScrutinyController()
	{
		cm = new GlobalFunction();
	}
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String adminHome() {
		
		return "/scrutiny/home";

	}
	@RequestMapping(value = "/caveats", method = RequestMethod.GET)
	public String caveats() {		
		return "/scrutiny/caveats";
	}
	
	@RequestMapping(value = "/applications", method = RequestMethod.GET)
	public String appliations() {		
		return "/scrutiny/applications";
	}
	
	@RequestMapping(value = "/cases", method = RequestMethod.GET)
	public String cases() {		
		return "/scrutiny/cases";
	}
	
	@RequestMapping(value = "/caveat/{id}", method = RequestMethod.GET)
	public String caveatView(Model model, @PathVariable Long id,HttpSession session) {
		String viewname="/content/access_denied";
		
		String message="This file has been assigned to other user";
		
		User user=(User) session.getAttribute("USER");
		
		String role=user.getUserroles().get(0).getLk().getLk_longname();
		Caveat cav= caveatService.getByPk(id);
		if(role.equals("CaseScrutiny")  && (cav.getCav_stage_lid().longValue()==1000036L || cav.getCav_stage_lid().longValue()==1000039L || cav.getCav_stage_lid().longValue()==1000040L) ){
			
			if(cav.getCav_assign_to()==null || cav.getCav_assign_to().longValue()==user.getUm_id().longValue()){
				cav.setCav_assign_to(user.getUm_id());
				caveatService.save(cav);
				viewname="/scrutiny/caveatView";
			}
			if(cav.getCav_assign_to().longValue()!=user.getUm_id().longValue()){
				viewname="/content/access_denied";
			}
			
		}
		if(role.equals("CaseSupervisor")  && (cav.getCav_stage_lid().longValue()==1000037L || cav.getCav_stage_lid().longValue()==1000038L) ){
			
			if(cav.getCav_assign_to()==null || cav.getCav_assign_to().longValue()==user.getUm_id().longValue()){
				cav.setCav_assign_to(user.getUm_id());
				caveatService.save(cav);
				viewname="/scrutiny/caveatView";
			}
			if(cav.getCav_assign_to().longValue()!=user.getUm_id().longValue()){
				viewname="/content/access_denied";
			}
			
		}
		model.addAttribute("caseId", id);
		model.addAttribute("message", message);
		
		return viewname;
		
	}
	
	@RequestMapping(value = "/case/{id}", method = RequestMethod.GET)
	public String caseView(Model model, @PathVariable Long id,HttpSession session) {
		String viewname="/content/access_denied";
		
		String message="This file has been assigned to other user";
		
		User user=(User) session.getAttribute("USER");
		
		String role=user.getUserroles().get(0).getLk().getLk_longname();
		RegisteredCaseDetails rcd= caseFileService.getRegisterCase(id);
		if(role.equals("CaseScrutiny")  && (rcd.getRcd_stage_lid().longValue()==1000036L || rcd.getRcd_stage_lid().longValue()==1000039L || rcd.getRcd_stage_lid().longValue()==1000040L)){
			
			if(rcd.getRcd_assign_to()==null || rcd.getRcd_assign_to().longValue()==user.getUm_id().longValue()){
				rcd.setRcd_assign_to(user.getUm_id());
				caseFileService.saveCaseDetails(rcd);
				viewname="/scrutiny/caseView";
			}
			if(rcd.getRcd_assign_to().longValue()!=user.getUm_id().longValue()){
				viewname="/content/access_denied";
			}
			
		}
		if(role.equals("CaseSupervisor")  && (rcd.getRcd_stage_lid().longValue()==1000037L || rcd.getRcd_stage_lid().longValue()==1000038L)){
			
			if(rcd.getRcd_assign_to()==null || rcd.getRcd_assign_to().longValue()==user.getUm_id().longValue()){
				rcd.setRcd_assign_to(user.getUm_id());
				caseFileService.saveCaseDetails(rcd);
				viewname="/scrutiny/caseView";
			}
			if(rcd.getRcd_assign_to().longValue()!=user.getUm_id().longValue()){
				viewname="/content/access_denied";
			}
			
		}
		model.addAttribute("caseId", id);
		model.addAttribute("message", message);
		
		return viewname;
	}
	
	@RequestMapping(value = "/application/{id}", method = RequestMethod.GET)
	public String applicationView(Model model, @PathVariable Long id,HttpSession session) {
		String viewname="/content/access_denied";
		
		String message="This file has been assigned to other user";
		
		User user=(User) session.getAttribute("USER");
		
		String role=user.getUserroles().get(0).getLk().getLk_longname();
		Application app= applicationService.getByPk(id);
		if(role.equals("ApplicationScrutiny")  && (app.getAp_stage_lid().longValue()==1000036L || app.getAp_stage_lid().longValue()==1000039L || app.getAp_stage_lid().longValue()==1000040L) ){			
			if(app.getAp_assign_to()==null || app.getAp_assign_to().longValue()==user.getUm_id().longValue()){
				app.setAp_assign_to(user.getUm_id());
				applicationService.save(app);
				viewname="/scrutiny/applicationView";
			}
			if(app.getAp_assign_to().longValue()!=user.getUm_id().longValue()){
				viewname="/content/access_denied";
			}
			
		}
		if(role.equals("ApplicationSupervisor")  && (app.getAp_stage_lid().longValue()==1000037L || app.getAp_stage_lid().longValue()==1000038L) ){			
			if(app.getAp_assign_to()==null || app.getAp_assign_to().longValue()==user.getUm_id().longValue()){
				app.setAp_assign_to(user.getUm_id());
				applicationService.save(app);
				viewname="/scrutiny/applicationView";
			}
			if(app.getAp_assign_to().longValue()!=user.getUm_id().longValue()){
				viewname="/content/access_denied";
			}
			
		}
		model.addAttribute("caseId", id);
		model.addAttribute("message", message);
		
		return viewname;
	}
	
	@RequestMapping(value="/copyFile",method=RequestMethod.GET)
	@ResponseBody
	public String copyFiles(HttpServletRequest request)
	{
		String jsonData = null;
		
		String doc_name=request.getParameter("pu_document_name");
		
		ActionResponse<IndexField> response= new ActionResponse<IndexField>();
		
	
		Lookup lookUp=lookupService.getLookUpObject("DRAFT_PATH");	
		String draft_path=lookUp.getLk_longname();

		File source = new File(draft_path+File.separator+doc_name);	
		String uploadPath = context.getRealPath("");
		
		doc_name="case_"+doc_name;
		File dest = new File(uploadPath+"/uploads/"+doc_name);

		try {
			    FileUtils.copyFile(source, dest);
			    response.setResponse("TRUE");
			    response.setData(doc_name);
			} 
			catch (IOException e) {
			    e.printStackTrace();
			    response.setResponse("FALSE");
			}
		jsonData = cm.convert_to_json(response);
		return jsonData;
	}
	@RequestMapping(value="/copyCaveatFile",method=RequestMethod.GET)
	@ResponseBody
	public String copyCaveatFile(HttpServletRequest request)
	{
		String jsonData = null;
		
		String doc_name=request.getParameter("cd_document_name");
		
		ActionResponse<IndexField> response= new ActionResponse<IndexField>();
		
	
		Lookup lookUp=lookupService.getLookUpObject("CAVEAT_PATH");	
		String draft_path=lookUp.getLk_longname();

		File source = new File(draft_path+File.separator+doc_name);	
		String uploadPath = context.getRealPath("");
		
		doc_name="cav_"+doc_name;
		File dest = new File(uploadPath+"/uploads/"+doc_name);

		try {
			    FileUtils.copyFile(source, dest);
			    response.setResponse("TRUE");
			    response.setData(doc_name);
			} 
			catch (IOException e) {
			    e.printStackTrace();
			    response.setResponse("FALSE");
			}
		jsonData = cm.convert_to_json(response);
		return jsonData;
	}
	@RequestMapping(value="/copyApplicationFile",method=RequestMethod.GET)
	@ResponseBody
	public String copyApplicationFile(HttpServletRequest request)
	{
		String jsonData = null;
		
		String doc_name=request.getParameter("au_document_name");
		
		ActionResponse<IndexField> response= new ActionResponse<IndexField>();
		
	
		Lookup lookUp=lookupService.getLookUpObject("APPLICATION_PATH");	
		String draft_path=lookUp.getLk_longname();

		File source = new File(draft_path+File.separator+doc_name);	
		String uploadPath = context.getRealPath("");
		doc_name="appl_"+doc_name;
		File dest = new File(uploadPath+"/uploads/"+doc_name);

		try {
			    FileUtils.copyFile(source, dest);
			    response.setResponse("TRUE");
			    response.setData(doc_name);
			} 
			catch (IOException e) {
			    e.printStackTrace();
			    response.setResponse("FALSE");
			}
		jsonData = cm.convert_to_json(response);
		return jsonData;
	}

	@RequestMapping(value = "/getCheckList", method = RequestMethod.GET)
	@ResponseBody
	public String getCheckList(HttpServletRequest request) 
	{
		String jsonData = null;
		String type=request.getParameter("type");
		ActionResponse<CheckList> response = new ActionResponse<CheckList>();
		
		List<CheckList> cl = scrutinyService.getCheckList(type);
		response.setResponse("TRUE");

		if (response != null) {
			response.setModelList(cl);
			jsonData = cm.convert_to_json(response);

		}
		return jsonData;

	}
	
	@RequestMapping(value = "/submit_case_file", method = RequestMethod.POST,consumes = {"application/xml", "application/json"})
	public @ResponseBody String submitCaseFile(@RequestBody RegisteredCaseDetails rcdetails,HttpSession session)
	 {
		User u = (User) session.getAttribute("USER");

		String jsonData = null;
		ActionResponse<RegisteredCaseDetails> response = new ActionResponse();
		
		RegisteredCaseDetails rcd=caseFileService.getRegisterCase(rcdetails.getRcd_id());
		
		List<CaseCheckListMapping> mappingold = scrutinyService.getCaseCheckList(rcdetails.getRcd_id());
		for(CaseCheckListMapping mapping:mappingold){
			mapping.setCm_rec_status(2);
			scrutinyService.saveCheckList(mapping);
		}
		// save stamp reporter data
		StampReporterData srd=rcdetails.getStampReporterData();
		if(srd!=null){
			srd.setSrd_rcd_mid(rcd.getRcd_id());
			srd.setSrd_cr_by(u.getUm_id());
			srd.setSrd_cr_date(new Date());
			if(rcdetails.getCheckList().isEmpty() && srd.getSrd_defective() && srd.getDefective_case_type()!=null)
			{
				rcd.setRcd_ct_id(srd.getDefective_case_type());				
			}
			srd=scrutinyService.saveStampReporterData(srd);
		}
		
		if(rcdetails.getCheckList().isEmpty())
		{
			rcd.setRcd_assign_to(null);
			rcd.setRcd_stage_lid(rcdetails.getRcd_stage_lid());
						
			caseFileService.saveCaseDetails(rcd);
			
			CaseFileStage cfs=new CaseFileStage();
			
			cfs.setRcs_rcd_mid(rcd.getRcd_id());
			cfs.setRcs_stage_lid(rcdetails.getRcd_stage_lid());
			cfs.setRcs_cr_by(u.getUm_id());
			cfs.setRcs_cr_date(new Date());
			
			caseFileStageService.save(cfs);
			
			response.setResponse("SUBMIT");
			
		}
		else
		{
			for(CheckList obj:rcdetails.getCheckList())
			{
				if(obj.getCheckbox()==true)
				   {
					CaseCheckListMapping cclp=new CaseCheckListMapping();
					
					cclp.setCm_rcd_mid(rcd.getRcd_id());
					cclp.setCm_checklist_id(obj.getC_id());
					cclp.setCm_cr_by(u.getUm_id());
					cclp.setCm_cr_date(new Date());
					cclp.setCm_rec_status(1);
					cclp.setCm_remark(obj.getC_remark());
					scrutinyService.saveCheckList(cclp);
					
				   }
			  }	
			
			rcd.setRcd_assign_to(null);
			rcd.setRcd_stage_lid(rcdetails.getRcd_stage_lid());
			
			caseFileService.saveCaseDetails(rcd);
			
			CaseFileStage cfs=new CaseFileStage();
			
			cfs.setRcs_rcd_mid(rcd.getRcd_id());
			cfs.setRcs_stage_lid(rcdetails.getRcd_stage_lid());
			cfs.setRcs_cr_by(u.getUm_id());
			cfs.setRcs_cr_date(new Date());
			
			caseFileStageService.save(cfs);
			
			response.setResponse("REJECT");
		}
		  
		jsonData=cm.convert_to_json(response);

		return jsonData;
	 }
	
	@RequestMapping(value = "/submit_caveat_file", method = RequestMethod.POST,consumes = {"application/xml", "application/json"})
	public @ResponseBody String submitCaveatFile(@RequestBody Caveat cavDetails,HttpSession session)
	 {
		User u = (User) session.getAttribute("USER");

		String jsonData = null;
		ActionResponse<Caveat> response = new ActionResponse();
		
		Caveat cav=caveatService.getByPk(cavDetails.getCav_id());
		
		List<CaveatCheckListMapping> mappingold = scrutinyService.getCaveatCheckList(cavDetails.getCav_id());
		for(CaveatCheckListMapping mapping:mappingold){
			mapping.setCm_rec_status(2);
			scrutinyService.saveCaveatCheckList(mapping);
		}
		
		if(cavDetails.getCheckList().isEmpty())
		{
			//Lookup lkStage=lookupService.getLookup("ECOURT_STAGE", "REPORTING_DONE");
			
			cav.setCav_assign_to(null);
			cav.setCav_stage_lid(cavDetails.getCav_stage_lid());//scrutiny done
			
			caveatService.save(cav);
			
			CaveatStage cs=new CaveatStage();
			cs.setCs_cav_mid(cav.getCav_id());
			cs.setCs_stage_lid(cavDetails.getCav_stage_lid());
			cs.setCs_cr_by(u.getUm_id());
			cs.setCs_cr_date(new Date());
			
			caveatService.saveCaveatStage(cs);
			
			response.setResponse("SUBMIT");
			
		}
		else
		{
			for(CheckList obj:cavDetails.getCheckList())
			{
				if(obj.getCheckbox()==true)
				   {
					CaveatCheckListMapping cclp=new CaveatCheckListMapping();
					
					cclp.setCm_cav_mid(cav.getCav_id());
					cclp.setCm_checklist_id(obj.getC_id());
					cclp.setCm_cr_by(u.getUm_id());
					cclp.setCm_cr_date(new Date());
					cclp.setCm_rec_status(1);
					cclp.setCm_remark(obj.getC_remark());
					scrutinyService.saveCaveatCheckList(cclp);
					
				   }
			  }	
			//Lookup lkStage=lookupService.getLookup("ECOURT_STAGE", "SUPERVISIOR_DEFECTS");
			cav.setCav_assign_to(null);
			cav.setCav_stage_lid(cavDetails.getCav_stage_lid());
			
			caveatService.save(cav);
			
			CaveatStage cs=new CaveatStage();
			cs.setCs_cav_mid(cav.getCav_id());
			cs.setCs_stage_lid(cavDetails.getCav_stage_lid());
			cs.setCs_cr_by(u.getUm_id());
			cs.setCs_cr_date(new Date());
			
			caveatService.saveCaveatStage(cs);
			
			response.setResponse("REJECT");
		}
		  
		jsonData=cm.convert_to_json(response);

		return jsonData;
	 }

	@RequestMapping(value = "/submit_application_file", method = RequestMethod.POST,consumes = {"application/xml", "application/json"})
	public @ResponseBody String submitApplicationFile(@RequestBody Application application,HttpSession session)
	 {
		User u = (User) session.getAttribute("USER");

		String jsonData = null;
		ActionResponse<Caveat> response = new ActionResponse();
		
		Application app=applicationService.getByPk(application.getAp_id());
		
		List<ApplicationCheckListMapping> mappingold = applicationService.getApplicationCheckList(application.getAp_id());
		for(ApplicationCheckListMapping mapping:mappingold){
			mapping.setCm_rec_status(2);
			scrutinyService.saveApplicationCheckList(mapping);
		}
		
		if(application.getCheckList().isEmpty())
		{
			//Lookup lkStage=lookupService.getLookup("ECOURT_STAGE", "REPORTING_DONE");
			
			app.setAp_assign_to(null);
			app.setAp_stage_lid(application.getAp_stage_lid());//scrutiny done
			
			applicationService.save(app);
			
			ApplicationStage cfs=new ApplicationStage();
			
			cfs.setAs_ap_mid(app.getAp_id());
			cfs.setAs_stage_lid(application.getAp_stage_lid());
			cfs.setAs_cr_by(u.getUm_id());
			cfs.setAs_cr_date(new Date());
			
			caseFileStageService.saveApplication(cfs);
			
			response.setResponse("SUBMIT");
			
		}
		else
		{
			for(CheckList obj:application.getCheckList())
			{
				if(obj.getCheckbox()==true)
				   {
					ApplicationCheckListMapping cclp=new ApplicationCheckListMapping();
					
					cclp.setCm_ap_mid(app.getAp_id());
					cclp.setCm_checklist_id(obj.getC_id());
					cclp.setCm_cr_by(u.getUm_id());
					cclp.setCm_cr_date(new Date());
					cclp.setCm_rec_status(1);
					cclp.setCm_remark(obj.getC_remark());
					scrutinyService.saveApplicationCheckList(cclp);					
				   }
			  }	
			//Lookup lkStage=lookupService.getLookup("ECOURT_STAGE", "SUPERVISIOR_DEFECTS");
			app.setAp_assign_to(null);
			app.setAp_stage_lid(application.getAp_stage_lid());
			
			applicationService.save(app);
			
			ApplicationStage cfs=new ApplicationStage();
			
			cfs.setAs_ap_mid(app.getAp_id());
			cfs.setAs_stage_lid(application.getAp_stage_lid());
			cfs.setAs_cr_by(u.getUm_id());
			cfs.setAs_cr_date(new Date());
			
			caseFileStageService.saveApplication(cfs);
			
			response.setResponse("REJECT");
		}
		  
		jsonData=cm.convert_to_json(response);

		return jsonData;
	 }
	
	@RequestMapping(value = "/getCaseList", method = RequestMethod.GET)
	public @ResponseBody String getCaseList(HttpSession session) {
		String jsonData = null;
		
		User user=(User) session.getAttribute("USER");
		String stageId="";
		//List<RegisteredCaseDetails> newDraftList = new ArrayList<RegisteredCaseDetails>();
		String role=user.getUserroles().get(0).getLk().getLk_longname();
			if(role.equals("CaseScrutiny")){
				stageId="1000036";
			}
			if(role.equals("CaseSupervisor")){
				stageId="1000038,1000037";
			}
		List<RegisteredCaseDetails> draftDetails = scrutinyService.getCasesByStage(stageId);
		ActionResponse<RegisteredCaseDetails> response = new ActionResponse<RegisteredCaseDetails>();

		int draftcount = 0;
		draftcount = draftDetails.size();

		if (draftDetails != null && draftDetails.size() != 0)
			response.setResponse("TRUE");
		response.setData(draftcount);
		response.setModelList(draftDetails);

		jsonData = cm.convert_to_json(response);

		return jsonData;
	}
	
	@RequestMapping(value = "/getRejectedCaseList", method = RequestMethod.GET)
	public @ResponseBody String getRejectedCaseList(HttpSession session) {
		String jsonData = null;
		
		User user=(User) session.getAttribute("USER");
		String stageId="";
		//List<RegisteredCaseDetails> newDraftList = new ArrayList<RegisteredCaseDetails>();
		String role=user.getUserroles().get(0).getLk().getLk_longname();
			if(role.equals("CaseScrutiny")){
				stageId="1000039,1000040";
			}
		List<RegisteredCaseDetails> draftDetails = scrutinyService.getCasesByStage(stageId);
		ActionResponse<RegisteredCaseDetails> response = new ActionResponse<RegisteredCaseDetails>();

		int draftcount = 0;
		draftcount = draftDetails.size();

		if (draftDetails != null && draftDetails.size() != 0)
			response.setResponse("TRUE");
		response.setData(draftcount);
		response.setModelList(draftDetails);

		jsonData = cm.convert_to_json(response);

		return jsonData;
	}
	
	@RequestMapping(value = "/getCaveatList", method = RequestMethod.GET)
	public @ResponseBody String getCaveatList(HttpSession session) {
		String jsonData = null;
		
		User user=(User) session.getAttribute("USER");
		String stageId="";
		String role=user.getUserroles().get(0).getLk().getLk_longname();
			if(role.equals("CaseScrutiny")){
				stageId="1000036";
			}
			if(role.equals("CaseSupervisor")){
				stageId="1000038,1000037";
			}
		//List<Caveat> newDraftList = new ArrayList<Caveat>();

		List<Caveat> draftDetails = scrutinyService.getCaveatsByStage(stageId);
		ActionResponse<Caveat> response = new ActionResponse<Caveat>();

		int draftcount = 0;
		draftcount = draftDetails.size();

		System.out.println(draftcount);

		if (draftDetails != null && draftDetails.size() != 0)
			response.setResponse("TRUE");
		response.setData(draftcount);
		response.setModelList(draftDetails);

		jsonData = cm.convert_to_json(response);

		return jsonData;
	}
	
	@RequestMapping(value = "/getRejectedCaveats", method = RequestMethod.GET)
	public @ResponseBody String getRejectedCaveatList(HttpSession session) {
		String jsonData = null;
		
		User user=(User) session.getAttribute("USER");

		//List<Caveat> newDraftList = new ArrayList<Caveat>();
		String stageId="";

		String role=user.getUserroles().get(0).getLk().getLk_longname();
			if(role.equals("CaseScrutiny")){
				stageId="1000039,1000040";
			}
		List<Caveat> draftDetails = scrutinyService.getCaveatsByStage(stageId);
		ActionResponse<Caveat> response = new ActionResponse<Caveat>();

		int draftcount = 0;
		draftcount = draftDetails.size();

		System.out.println(draftcount);

		if (draftDetails != null && draftDetails.size() != 0)
			response.setResponse("TRUE");
		response.setData(draftcount);
		response.setModelList(draftDetails);

		jsonData = cm.convert_to_json(response);

		return jsonData;
	}

	@RequestMapping(value = "/getApplicationList", method = RequestMethod.GET)
	public @ResponseBody String getApplicationList(HttpSession session) {
		String jsonData = null;
		
		User user=(User) session.getAttribute("USER");
		String stageId="";
		String role=user.getUserroles().get(0).getLk().getLk_longname();
			if(role.equals("ApplicationScrutiny")){
				stageId="1000036";
			}
			if(role.equals("ApplicationSupervisor")){
				stageId="1000038,1000037";
			}
		List<Application> newApplicationList= scrutinyService.getApplicationsByStage(stageId);
		ActionResponse<Application> response = new ActionResponse<Application>();

		if (newApplicationList != null && newApplicationList.size() != 0)
			response.setResponse("TRUE");

		response.setModelList(newApplicationList);

		jsonData = cm.convert_to_json(response);

		return jsonData;
	}
	@RequestMapping(value = "/getRejectedApplications", method = RequestMethod.GET)
	public @ResponseBody String getRejectedApplications(HttpSession session) {
		String jsonData = null;
		
		User user=(User) session.getAttribute("USER");
		String stageId="";

		String role=user.getUserroles().get(0).getLk().getLk_longname();
			if(role.equals("ApplicationScrutiny")){
				stageId="1000039,1000040";
			}
		List<Application> newApplicationList= scrutinyService.getApplicationsByStage(stageId);
		ActionResponse<Application> response = new ActionResponse<Application>();

		if (newApplicationList != null && newApplicationList.size() != 0)
			response.setResponse("TRUE");

		response.setModelList(newApplicationList);

		jsonData = cm.convert_to_json(response);

		return jsonData;
	}

	@RequestMapping(value = "/getStampReporterData", method = RequestMethod.GET)
	@ResponseBody
	public String getStampReporterData(HttpServletRequest request) {

		String id = request.getParameter("docId");
		StampReporterData response = null;

		Long doc = new Long(id);
		ActionResponse<StampReporterData> pd = new ActionResponse<StampReporterData>();
		String jsonData = null;
		
		
		response = scrutinyService.getStampReporterData(doc);
		 
		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelData(response);
			jsonData = cm.convert_to_json(pd);
		}
		return jsonData;
	}
	@RequestMapping(value = "/getCaseHistory", method = RequestMethod.GET)
	@ResponseBody
	public String getCaseHistory(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<CaseCheckListMapping> mapping = null;

		Long doc = new Long(id);
		ActionResponse<CaseCheckListMapping> response = new ActionResponse<CaseCheckListMapping>();
		String jsonData = null;

		mapping = caseFileService.getCaseReportingHistory(doc);

		if (mapping != null) {
			response.setModelList(mapping);
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);

		}
		return jsonData;

	}
	@RequestMapping(value = "/getCaveatHistory", method = RequestMethod.GET)
	@ResponseBody
	public String getCaveatHistory(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<CaveatCheckListMapping> mapping = null;

		Long doc = new Long(id);
		ActionResponse<CaveatCheckListMapping> response = new ActionResponse<CaveatCheckListMapping>();
		String jsonData = null;

		mapping = caveatService.getCaveatReportingHistory(doc);

		if (mapping != null) {
			response.setModelList(mapping);
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);

		}
		return jsonData;

	}
	@RequestMapping(value = "/getApplicationHistory", method = RequestMethod.GET)
	@ResponseBody
	public String getApplicationHistory(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<ApplicationCheckListMapping> mapping = null;

		Long doc = new Long(id);
		ActionResponse<ApplicationCheckListMapping> response = new ActionResponse<ApplicationCheckListMapping>();
		String jsonData = null;

		mapping = applicationService.getApplicationReportingHistory(doc);

		if (mapping != null) {
			response.setModelList(mapping);
			response.setResponse("TRUE");
			jsonData = cm.convert_to_json(response);

		}
		return jsonData;

	}
	
	@RequestMapping(value = "/getCaseTypes", method = RequestMethod.GET)
	@ResponseBody
	public String getCasetypes() {
		String jsonData = null;

		ActionResponse<CaseType> response = new ActionResponse<CaseType>();

		List<CaseType> caseTypes = scrutinyService.getCasetypes();

		response.setModelList(caseTypes);
		if (response != null) {
			jsonData = cm.convert_to_json(response);
		}

		return jsonData;
	}


}
