package com.dms.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.dms.model.ActionResponse;
import com.dms.model.CSVUpload;
import com.dms.model.CSVUploadStage;
import com.dms.model.CSVUtils;
import com.dms.model.CaseType;
import com.dms.model.Caveat;
import com.dms.model.CaveatCourtFee;
import com.dms.model.CaveatDocuments;
import com.dms.model.CaveatPetitionerDetails;
import com.dms.model.CaveatRespondentDetails;
import com.dms.model.CaveatStage;
import com.dms.model.IndexField;
import com.dms.model.Lookup;
import com.dms.model.LowerCourtCaseType;
import com.dms.model.Search;
import com.dms.model.State;
import com.dms.model.User;
import com.dms.service.CSVUploadService;
import com.dms.service.CaseFileStageService;
import com.dms.service.CaveatService;
import com.dms.service.LookupService;
import com.dms.service.UserRoleService;
import com.dms.utility.GlobalFunction;
import com.lowagie.text.DocumentException;

@Controller
@RequestMapping("/caveat")
public class CaveatController 
{
	private GlobalFunction globalfunction;
	
	@Autowired
	private CaveatService caveatService;
	@Autowired
	ServletContext context;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private CSVUploadService CSVUploadService;
		
	private Integer uploadCount=0;
	
	public CaveatController()
	{
		globalfunction = new GlobalFunction();
	}
	@RequestMapping(value = "/manage", method = RequestMethod.GET)
	public String Manage() {

		return "/caveat/manage";
	}
	@RequestMapping(value = "/getCaveatList", method = RequestMethod.POST)
	public @ResponseBody String getCaseList(@RequestBody Search search, HttpSession session) {
		String jsonData = null;
		
		User user=(User) session.getAttribute("USER");
		Long stageId=search.getStageId();
		String diary_no="";
		diary_no=search.getDiary_no();
		String stageDate=null;
		if(search.getStageDate()!=null)
		{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			stageDate = formatter.format(search.getStageDate());
		}
		
		List<Caveat> newDraftList = new ArrayList<Caveat>();
		Lookup lkreporting=lookupService.getLookup("ECOURT_STAGE", "REPORTING_DONE");
		List<Caveat> draftDetails= new ArrayList<Caveat>();
		if(diary_no!=null){
			Caveat c=caveatService.getByDiaryNo(diary_no);
			draftDetails.add(c);
		}else{
			draftDetails=caveatService.searchCaveatFiles(stageId,stageDate);
		}
		ActionResponse<Caveat> response = new ActionResponse<Caveat>();

		CaveatPetitionerDetails pDetails =null;

		CaveatRespondentDetails rDetails = null;		

		for (Caveat c : draftDetails) {
			pDetails= new CaveatPetitionerDetails();
			rDetails= new CaveatRespondentDetails();
			pDetails =caveatService.getFirstPetitioner(c.getCav_id());
			rDetails=caveatService.getFirstRespondent(c.getCav_id());
			        if(pDetails.getCpt_id()!=null){
			         c.setPetitionerDetails(pDetails);
			        }
			        if(rDetails.getCrt_id()!=null) {
			          c.setRespondentDetails(rDetails);
			        }		
			newDraftList.add(c);
		}


		if (newDraftList != null && newDraftList.size() != 0){
			response.setResponse("TRUE");
			response.setModelList(newDraftList);
		}else{
			response.setResponse("FALSE");
			}
		
		jsonData = globalfunction.convert_to_json(response);

		return jsonData;
	}
	@RequestMapping(value="/downloadCSV",method=RequestMethod.GET)
	public void downloadCaseFile(HttpServletRequest request,HttpServletResponse response,HttpSession session)
	{
		String date=request.getParameter("date");
		String case_no_generated=request.getParameter("case_no_generated");
		Lookup lkreporting=lookupService.getLookup("ECOURT_STAGE","REPORTING_DONE");	
			List<Caveat> caseLists=caveatService.searchCaveatFilesForCSV(lkreporting.getLk_id());
		
			Lookup lookUp=lookupService.getLookUpObject("CSV_DOWNLOAD_PATH");
			String zip_download_path=lookUp.getLk_longname();
			Date date1 = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String folder = "caveat_"+formatter.format(date1);
			String dest_folder=zip_download_path+File.separator+folder;
			
			File destFolder = new File(dest_folder);
			
			if(!destFolder.exists())
			{
				destFolder.mkdir();
			}
			
			generateCaveatfiles(dest_folder,caseLists,lkreporting);
			generatePetitioners(dest_folder,caseLists);
			generateRespondents(dest_folder,caseLists);
			generateCourtFees(dest_folder,caseLists);
			 try 
				{
		        	zipFolder(destFolder.getAbsolutePath(),destFolder.getAbsolutePath()+".zip");
				} 
				catch (Exception e) {
				    e.printStackTrace();
				}
		        
		        // to download file
		        try
		        {
		        response.setContentType("application/zip");  
		        PrintWriter out = response.getWriter();  
		        String filename = destFolder.getName()+".zip";   
		        String filepath = destFolder.getAbsolutePath()+".zip";  
		        response.setContentType("APPLICATION/OCTET-STREAM");   
		        response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
		          
		        FileInputStream fileInputStream = new FileInputStream(filepath);  
		                    
		        int i;   
		        while ((i=fileInputStream.read()) != -1) {  
		        out.write(i);   
		        }   
		        fileInputStream.close();   
		        out.close(); 
		        }catch(Exception e)
		        {
		        	e.printStackTrace();
		        }
			
	    }
	private void generateCourtFees(String dest_folder, List<Caveat> caseLists) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"courtfees.csv";
		try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "ReceiptNo", "Amount","Date"));

			for (Caveat c : caseLists) {
				
				List<CaveatCourtFee> fees=caveatService.getCourtFee(c.getCav_id());
				for (CaveatCourtFee cf : fees) {
					
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					String courtFeeDate = formatter.format(cf.getCcf_date());
					List<String> list = new ArrayList<>();
					list.add(c.getCav_draft_no());
				    list.add(c.getCav_diary_no());
				    list.add(cf.getCcf_receipt_no());
				    list.add(String.valueOf(cf.getCcf_amount()));
				    list.add(courtFeeDate);
				    CSVUtils.writeLine(writer, list);
				}
				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void generateRespondents(String dest_folder, List<Caveat> caseLists) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"respondents.csv";
		
        try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "Name", "Email","Address","Mobile","Pincode","City","Other Contacts"));

			for (Caveat c : caseLists) {
				List<CaveatRespondentDetails> respondents=caveatService.getRespondent(c.getCav_id());
				for (CaveatRespondentDetails rd : respondents) {
					List<String> list = new ArrayList<>();
					list.add(c.getCav_draft_no());
					list.add(c.getCav_diary_no());
					list.add(rd.getCrt_name());
					list.add(rd.getCrt_email());
			    	list.add(rd.getCrt_address());
			    	list.add(String.valueOf(rd.getCrt_mobile()));
			    	list.add(String.valueOf(rd.getCrt_pincode()));
			    	list.add(rd.getCrt_city());
			    	list.add(rd.getCrt_other_contact());
			    	
			    	CSVUtils.writeLine(writer, list);
				}
				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void generatePetitioners(String dest_folder, List<Caveat> caseLists) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"petitioners.csv";
		
        try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "Name", "Email","Address","Mobile","Pincode","City","Other Contacts"));

			for (Caveat c : caseLists) {
				List<CaveatPetitionerDetails> petitioners=caveatService.getPetitioner(c.getCav_id());
				for (CaveatPetitionerDetails p : petitioners) {
					List<String> list = new ArrayList<>();
					list.add(c.getCav_draft_no());
					list.add(c.getCav_diary_no());
					list.add(p.getCpt_name());
			    	list.add(p.getCpt_email());
			    	list.add(p.getCpt_address());
			    	list.add(String.valueOf(p.getCpt_mobile()));
			    	list.add(String.valueOf(p.getCpt_pincode()));
			    	list.add(p.getCpt_city());
			    	list.add(p.getCpt_other_contact());
			    	
			    	CSVUtils.writeLine(writer, list);
				}
				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void generateCaveatfiles(String dest_folder, List<Caveat> caseLists,Lookup lkreporting) {
		// TODO Auto-generated method stub
		String csvFile = dest_folder+File.separator+"caveats.csv";
		Lookup lkdraftSubmit=lookupService.getLookup("ECOURT_STAGE","REPORTING_DONE");
        try {
			FileWriter writer = new FileWriter(csvFile);


			//for header
			CSVUtils.writeLine(writer, Arrays.asList("DraftNo","DiaryNo", "CaseType", "District","Fullname","Username","DOF","CaveatorName","CaveatorEmail","CaveatorMobile","CourtType", "CaseType","CaseNo","CaseYear","State","District","LowerCourtType","JudgeName","DecisionDate"));

			for (Caveat c : caseLists) {
				CaveatStage stage=caveatService.getByStage(c.getCav_id(),lkdraftSubmit.getLk_id());
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				String createdDate = formatter.format(stage.getCs_cr_date());
				String decisionDate = "";
				if(c.getCav_judgmnt_date()!=null)
					decisionDate=formatter.format(c.getCav_judgmnt_date());
				
			    List<String> list = new ArrayList<>();
			    list.add(c.getCav_draft_no());
			    list.add(c.getCav_diary_no());			    
			    list.add(c.getCaseType().getCt_label().toString());
			    list.add(String.valueOf(c.getCav_dist_mid()));
			    list.add(c.getUserMaster().getUm_fullname());
			    if(c.getUserMaster().getUserroles().get(0).getLk().getLk_longname().equals("Advocate"))
			    	list.add(c.getUserMaster().getUsername());
			    else
			    	list.add("");			    
			    list.add(createdDate);
			    list.add(c.getCav_caveator_name());
			    list.add(c.getCav_email());
			    list.add(c.getCav_mobile());
			    if(c.getCav_court()==1L)
			    	list.add("LC");
			    else
			    	list.add("HC");
			    if(c.getCav_court()==1L){
			    	LowerCourtCaseType lct=caveatService.getLCCaseTypeById(c.getCav_lc_case_type());
			    	list.add(lct.getCt_name());
			    }
			    else{				    	
			    	CaseType ct=caveatService.getCaseTypeById(c.getCav_hc_case_type());
			    	list.add(ct.getCt_label());
			    }
			    list.add(String.valueOf(c.getCav_lc_case_no()));
			    list.add(String.valueOf(c.getCav_lc_case_year()));
			    if(c.getCav_state_mid()!=null){
			    	State state=caveatService.getStateById(c.getCav_state_mid());
			    	list.add(state.getS_name());
			    }else{
			    	list.add("");	
			    }
			    list.add(String.valueOf(c.getCav_ord_dist()));
			    list.add(c.getCourtType().getLct_name());
			    list.add(c.getCav_ord_psd_by());
			    list.add(decisionDate);
			    
			    CSVUtils.writeLine(writer, list);

				//try custom separator and quote.
				//CSVUtils.writeLine(writer, list, '|', '\"');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void zipFolder(String srcFolderPath, String zipPath) throws Exception 
    {
    	 File zipFile=new File(zipPath);
    	 final Path sourceFolderPath=Paths.get(srcFolderPath);
        final ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile));
        Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>()
        		{
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                zos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));
                Files.copy(file, zos);
                zos.closeEntry();
                return FileVisitResult.CONTINUE;
            }
        });
        zos.close();
    }
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody String create(MultipartHttpServletRequest request,HttpSession session) throws DocumentException 
	{
		this.uploadCount=0;
		ActionResponse<Caveat> response = new ActionResponse();
		User u=(User) session.getAttribute("USER");
		String jsonData="";
		Lookup lookup=lookupService.getLookUpObject("CSV_UPLOAD_PATH");
		MultipartFile mpf = null;
    	Iterator<String> itr = request.getFileNames();
    	String csvPath="";
    	Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String currentDate = formatter.format(date);
    	while (itr.hasNext()) 
		{
			mpf = request.getFile(itr.next());
			CSVUpload csvupload=new CSVUpload();
			csvupload.setCu_cr_by(u.getUm_id());
			csvupload.setCu_cr_date(date);
			csvupload.setCu_name(mpf.getName());
			csvupload.setCu_type("C");
			csvupload=CSVUploadService.save(csvupload);
			
			String filename="caveat"+"_"+csvupload.getCu_id()+".csv";
			csvupload.setCu_name(filename);
			csvupload=CSVUploadService.save(csvupload);
			
			csvPath=lookup.getLk_longname() + File.separator +filename;
			try {
				FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(csvPath));
				updateCaseNo(csvPath,csvupload, u);
			}catch (IOException e) {
				e.printStackTrace();
			}
			
		}
    	String message=uploadCount+" no of files has been updated";
    	
    	response.setResponse("TRUE");
    	response.setData(message);
    	jsonData = globalfunction.convert_to_json(response);
		return jsonData;
	}
	private void updateCaseNo(String csvPath,CSVUpload csvupload,User u) {
		 String csvFile = csvPath;
	        BufferedReader br = null;
	        String line = "";
	        String cvsSplitBy = ",";
	        Lookup lkcaseGenerated=lookupService.getLookup("ECOURT_STAGE", "CASE_NO_GENERATED");
	        try {

	            br = new BufferedReader(new FileReader(csvFile));
	            while ((line = br.readLine()) != null) {

	                // use comma as separator
	                String[] casefiles = line.split(cvsSplitBy);
	                //DraftNo,DiaryNo,CaveatNo,CaveatYear
	                System.out.println("DraftNo = " + casefiles[0] + " , caveateno=" + casefiles[2] + "]");
	                Caveat c=caveatService.getByDiaryNo(casefiles[1]);
	                CaveatStage cstemp=caveatService.getCaveatStage(c.getCav_id(), 1000043L);
	                if(cstemp==null){
	                c.setCav_no(Integer.parseInt(casefiles[2]));
	                c.setCav_year(Integer.parseInt(casefiles[3]));
	                c.setCav_stage_lid(lkcaseGenerated.getLk_id());
	                caveatService.save(c);
	                
	                CaveatStage cfs=new CaveatStage();
	                cfs.setCs_cr_by(u.getUm_id());
	                cfs.setCs_cr_date(new Date());
	                cfs.setCs_cav_mid(c.getCav_id());
	                cfs.setCs_stage_lid(lkcaseGenerated.getLk_id());
	                caveatService.saveCaveatStage(cfs);
	                
	                CSVUploadStage csvstage=new CSVUploadStage();
	                csvstage.setCus_cu_mid(csvupload.getCu_id());
	                csvstage.setCus_file_id(c.getCav_id());
	                CSVUploadService.save(csvstage);
	                uploadCount++;
	                }
	            }

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	}
	@RequestMapping(value = "/getRegisterCaveat", method = RequestMethod.GET)
	@ResponseBody
	public String getRegisterCaveat(HttpServletRequest request) {

		String id = request.getParameter("docId");
		Caveat cav = null;

		Long doc = new Long(id);
		ActionResponse<Caveat> response = new ActionResponse<Caveat>();
		String jsonData = null;

		CaveatRespondentDetails caveatRespondentDetails=null;
		CaveatPetitionerDetails caveatPetitionerDetails=null;
		
		cav = caveatService.getRegisterCaveat(doc);
		caveatPetitionerDetails=caveatService.getFirstPetitioner(doc);
		caveatRespondentDetails=caveatService.getFirstRespondent(doc);
		
		    if(caveatPetitionerDetails.getCpt_id()!=null){
		    	cav.setPetitionerDetails(caveatPetitionerDetails);
		    }
		    if(caveatRespondentDetails.getCrt_id()!=null){
		    	cav.setRespondentDetails(caveatRespondentDetails);
		    	
		    }
		    
		if (cav != null) {
			response.setResponse("TRUE");
			response.setModelData(cav);
			jsonData = globalfunction.convert_to_json(response);

		}
		return jsonData;

	}
	
	@RequestMapping(value = "/getCaveatPetitioner", method = RequestMethod.GET)
	@ResponseBody
	public String getCaveatPetitioner(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<CaveatPetitionerDetails> response = null;

		Long doc = new Long(id);
		ActionResponse<CaveatPetitionerDetails> pd = new ActionResponse<CaveatPetitionerDetails>();
		String jsonData = null;

		response = caveatService.getPetitioner(doc);

		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelList(response);
			jsonData = globalfunction.convert_to_json(pd);

		}
		return jsonData;

	}

	@RequestMapping(value = "/getCaveatRespondent", method = RequestMethod.GET)
	@ResponseBody
	public String getCaveatRespondent(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<CaveatRespondentDetails> response = null;

		Long doc = new Long(id);
		ActionResponse<CaveatRespondentDetails> pd = new ActionResponse<CaveatRespondentDetails>();
		String jsonData = null;

		response = caveatService.getRespondent(doc);

		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelList(response);
			jsonData = globalfunction.convert_to_json(pd);

		}
		return jsonData;

	}
	@RequestMapping(value = "/getCourtFee", method = RequestMethod.GET)
	@ResponseBody
	public String getCourtFee(HttpServletRequest request) {

		String id = request.getParameter("docId");
		List<CaveatCourtFee> response = null;

		Long doc = new Long(id);
		ActionResponse<CaveatCourtFee> pd = new ActionResponse<CaveatCourtFee>();
		String jsonData = null;

		response = caveatService.getCaveatCourtFee(doc);

		if (response != null) {
			pd.setResponse("TRUE");
			pd.setModelList(response);
			jsonData = globalfunction.convert_to_json(pd);

		}
		return jsonData;

	}
	
	@RequestMapping(value="/getUploadedDocuments", method=RequestMethod.GET)
	@ResponseBody
	public String getUploadedDocuments(HttpServletRequest request)
	{
		String jsonData=null;
		String file_id=request.getParameter("rcd_id");
		Long rcd_id=Long.valueOf(file_id);
		
		List<CaveatDocuments> documentList=caveatService.getUploadedCaveates(rcd_id);
		ActionResponse<CaveatDocuments> response=new ActionResponse<CaveatDocuments>();
		
		response.setModelList(documentList);
		response.setResponse("TRUE");
		
	     jsonData=globalfunction.convert_to_json(response);
	     return jsonData;
	}

	@RequestMapping(value = "/getCaveatStages", method = RequestMethod.GET)
	public @ResponseBody String getCaseStages(HttpServletRequest request) {
		String jsonData="";
		String id = request.getParameter("docId");
		Long docId = new Long(id);
		ActionResponse<CaveatStage> response=new ActionResponse<CaveatStage>();
		List<CaveatStage> stages=caveatService.getStages(docId);
		response.setData("TRUE");
		response.setModelList(stages);
		jsonData=globalfunction.convert_to_json(response);
		return jsonData;	
	}
	
	@RequestMapping(value="/copyCaveatFile",method=RequestMethod.GET)
	@ResponseBody
	public String copyCaveatFile(HttpServletRequest request)
	{
		String jsonData = null;
		ActionResponse<IndexField> response= new ActionResponse<IndexField>();
		
		String file_id=request.getParameter("cav_id");
		Long cav_id=Long.valueOf(file_id);
		
		List<CaveatDocuments> documentList=caveatService.getUploadedCaveates(cav_id);
		
		if(documentList.size()==1)
		{
			String doc_name=documentList.get(0).getCd_document_name();
			
			Lookup lookUp=lookupService.getLookUpObject("CAVEAT_PATH");	
			String draft_path=lookUp.getLk_longname();
	
			File source = new File(draft_path+File.separator+doc_name);	
			String uploadPath = context.getRealPath("");
			doc_name="cav_"+doc_name;
			File dest = new File(uploadPath+"/uploads/"+doc_name);
	
			try {
				    FileUtils.copyFile(source, dest);
				    response.setResponse("TRUE");
				    response.setData(doc_name);
				} 
				catch (IOException e) {
				    e.printStackTrace();
				    response.setResponse("FALSE");
				    response.setData("Error in displaying file");
				}
		}else
		{
			response.setResponse("FALSE");
			response.setData("File not found");
		}
		jsonData = globalfunction.convert_to_json(response);
		return jsonData;
	}
}
