package com.dms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "old_caveat")
public class CaveatOld {
	
	@Id
	private Long cav_id;
	
	@Column(name = "cav_no")
	private Integer cav_no;
	
	@Column(name="cav_year")
	private Integer cav_year;
	
	@Column(name = "cav_type")
	private Long cav_type;

	@Column(name = "cav_dist_mid")
	private Long cav_dist_mid;

	@Column(name = "cav_caveator_name")
	private String cav_caveator_name;

	@Column(name = "cav_lc_case_type")
	private String cav_lc_case_type;
	
	@Column(name = "cav_lc_case_no")
	private String cav_lc_case_no;
	
	@Column(name = "cav_lc_case_year")
	private Integer cav_lc_case_year;

	@Column(name = "cav_ord_psd_by")
	private String cav_ord_psd_by;

	@Column(name = "cav_ord_dist")
	private Long cav_ord_dist;

	@Column(name = "cav_judgmnt_date")
	private Date cav_judgmnt_date;

	@Column(name = "cav_cr_date")
	private Date cav_cr_date;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="cav_dist_mid",insertable=false,updatable=false)
	private District district;
	
//	@OneToOne(cascade = CascadeType.PERSIST)
//	@JoinColumn(name="cav_ord_dist",insertable=false,updatable=false)
//	private District orderDistrict;
	
	public Integer getCav_no() {
		return cav_no;
	}

	public void setCav_no(Integer cav_no) {
		this.cav_no = cav_no;
	}

	public Integer getCav_year() {
		return cav_year;
	}

	public void setCav_year(Integer cav_year) {
		this.cav_year = cav_year;
	}

	public Long getCav_type() {
		return cav_type;
	}

	public void setCav_type(Long cav_type) {
		this.cav_type = cav_type;
	}

	public Long getCav_dist_mid() {
		return cav_dist_mid;
	}

	public void setCav_dist_mid(Long cav_dist_mid) {
		this.cav_dist_mid = cav_dist_mid;
	}

	public String getCav_caveator_name() {
		return cav_caveator_name;
	}

	public void setCav_caveator_name(String cav_caveator_name) {
		this.cav_caveator_name = cav_caveator_name;
	}


	public String getCav_lc_case_type() {
		return cav_lc_case_type;
	}

	public void setCav_lc_case_type(String cav_lc_case_type) {
		this.cav_lc_case_type = cav_lc_case_type;
	}

	public String getCav_lc_case_no() {
		return cav_lc_case_no;
	}

	public void setCav_lc_case_no(String cav_lc_case_no) {
		this.cav_lc_case_no = cav_lc_case_no;
	}

	public Integer getCav_lc_case_year() {
		return cav_lc_case_year;
	}

	public void setCav_lc_case_year(Integer cav_lc_case_year) {
		this.cav_lc_case_year = cav_lc_case_year;
	}


	public String getCav_ord_psd_by() {
		return cav_ord_psd_by;
	}

	public void setCav_ord_psd_by(String cav_ord_psd_by) {
		this.cav_ord_psd_by = cav_ord_psd_by;
	}

	public Long getCav_ord_dist() {
		return cav_ord_dist;
	}

	public void setCav_ord_dist(Long cav_ord_dist) {
		this.cav_ord_dist = cav_ord_dist;
	}

	public Date getCav_judgmnt_date() {
		return cav_judgmnt_date;
	}

	public void setCav_judgmnt_date(Date cav_judgmnt_date) {
		this.cav_judgmnt_date = cav_judgmnt_date;
	}

	
	public Date getCav_cr_date() {
		return cav_cr_date;
	}

	public void setCav_cr_date(Date cav_cr_date) {
		this.cav_cr_date = cav_cr_date;
	}

	
	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

//	public District getOrderDistrict() {
//		return orderDistrict;
//	}
//
//	public void setOrderDistrict(District orderDistrict) {
//		this.orderDistrict = orderDistrict;
//	}

	public Long getCav_id() {
		return cav_id;
	}

	public void setCav_id(Long cav_id) {
		this.cav_id = cav_id;
	}
	
	
	
}
