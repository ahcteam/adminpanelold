package com.dms.model;

import javax.persistence.Entity;




public class DailyActivityReport {
	

	private String parameter1="0";
	
	private String parameter2="0";

	private String parameter3="0";
	
	private String parameter4="0";
	
	private String parameter5="0";
	
	private String parameter6="0";

	private String parameter7="0";

	private String parameter8="0";
	
	private String parameter9="0";

	private String parameter10="0";
	
	private String parameter11="0";

	private String parameter12="0";
	
	private String parameter13="0";

	private String parameter14="0";
	
	private int flag1;
	private int flag2;
	private int flag3;
	private int flag4;
	private int flag5;
	private int flag6;
	private int flag7;
	private int flag8;
	private int flag9;
	private int flag10;
	private int flag11;
	private int flag12;
	private int flag13;
	private int flag14;
	
	private String value1;
	private String value2;
	private String value3;
	private String value4;
	private String value5;
	private String value6;
	private String value7;
	private String value8;
	private String value9;
	private String value10;
	private String value11;
	private String value12;
	private String value13;
	private String value14;
	
	private String link1="";
	
	private String link2="";

	private String link3="";
	
	private String link4="";
	
	private String link5="";
	
	private String link6="";

	private String link7="";

	private String link8="";
	private String link9="";
	private String link10="";
	private String link11="";
	private String link12="";
	
	
	
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getValue2() {
		return value2;
	}
	public void setValue2(String value2) {
		this.value2 = value2;
	}
	public String getValue3() {
		return value3;
	}
	public void setValue3(String value3) {
		this.value3 = value3;
	}
	public String getValue4() {
		return value4;
	}
	public void setValue4(String value4) {
		this.value4 = value4;
	}
	public String getValue5() {
		return value5;
	}
	public void setValue5(String value5) {
		this.value5 = value5;
	}
	public String getValue6() {
		return value6;
	}
	public void setValue6(String value6) {
		this.value6 = value6;
	}
	public String getValue7() {
		return value7;
	}
	public void setValue7(String value7) {
		this.value7 = value7;
	}
	public String getValue8() {
		return value8;
	}
	public void setValue8(String value8) {
		this.value8 = value8;
	}
	public String getValue9() {
		return value9;
	}
	public void setValue9(String value9) {
		this.value9 = value9;
	}
	public String getValue10() {
		return value10;
	}
	public void setValue10(String value10) {
		this.value10 = value10;
	}
	public int getFlag1() {
		return flag1;
	}
	public void setFlag1(int flag1) {
		this.flag1 = flag1;
	}
	public int getFlag2() {
		return flag2;
	}
	public void setFlag2(int flag2) {
		this.flag2 = flag2;
	}
	public int getFlag3() {
		return flag3;
	}
	public void setFlag3(int flag3) {
		this.flag3 = flag3;
	}
	public int getFlag4() {
		return flag4;
	}
	public void setFlag4(int flag4) {
		this.flag4 = flag4;
	}
	public String getParameter1() {
		return parameter1;
	}
	public void setParameter1(String parameter1) {
		this.parameter1 = parameter1;
	}
	public String getParameter2() {
		return parameter2;
	}
	public void setParameter2(String parameter2) {
		this.parameter2 = parameter2;
	}
	public String getParameter3() {
		return parameter3;
	}
	public void setParameter3(String parameter3) {
		this.parameter3 = parameter3;
	}
	public String getParameter4() {
		return parameter4;
	}
	public void setParameter4(String parameter4) {
		this.parameter4 = parameter4;
	}
	public String getParameter5() {
		return parameter5;
	}
	public void setParameter5(String parameter5) {
		this.parameter5 = parameter5;
	}
	public String getParameter6() {
		return parameter6;
	}
	public void setParameter6(String parameter6) {
		this.parameter6 = parameter6;
	}
	public String getParameter7() {
		return parameter7;
	}
	public void setParameter7(String parameter7) {
		this.parameter7 = parameter7;
	}
	public String getParameter8() {
		return parameter8;
	}
	public void setParameter8(String parameter8) {
		this.parameter8 = parameter8;
	}
	public String getParameter9() {
		return parameter9;
	}
	public void setParameter9(String parameter9) {
		this.parameter9 = parameter9;
	}
	public String getParameter10() {
		return parameter10;
	}
	public void setParameter10(String parameter10) {
		this.parameter10 = parameter10;
	}
	public String getParameter11() {
		return parameter11;
	}
	public void setParameter11(String parameter11) {
		this.parameter11 = parameter11;
	}
	public String getParameter12() {
		return parameter12;
	}
	public void setParameter12(String parameter12) {
		this.parameter12 = parameter12;
	}
	public String getParameter13() {
		return parameter13;
	}
	public void setParameter13(String parameter13) {
		this.parameter13 = parameter13;
	}
	public String getParameter14() {
		return parameter14;
	}
	public void setParameter14(String parameter14) {
		this.parameter14 = parameter14;
	}
	public String getValue11() {
		return value11;
	}
	public void setValue11(String value11) {
		this.value11 = value11;
	}
	public String getValue12() {
		return value12;
	}
	public void setValue12(String value12) {
		this.value12 = value12;
	}
	public String getValue13() {
		return value13;
	}
	public void setValue13(String value13) {
		this.value13 = value13;
	}
	public String getValue14() {
		return value14;
	}
	public void setValue14(String value14) {
		this.value14 = value14;
	}
	public int getFlag5() {
		return flag5;
	}
	public void setFlag5(int flag5) {
		this.flag5 = flag5;
	}
	public int getFlag6() {
		return flag6;
	}
	public void setFlag6(int flag6) {
		this.flag6 = flag6;
	}
	public int getFlag7() {
		return flag7;
	}
	public void setFlag7(int flag7) {
		this.flag7 = flag7;
	}
	public int getFlag8() {
		return flag8;
	}
	public void setFlag8(int flag8) {
		this.flag8 = flag8;
	}
	public int getFlag9() {
		return flag9;
	}
	public void setFlag9(int flag9) {
		this.flag9 = flag9;
	}
	public int getFlag10() {
		return flag10;
	}
	public void setFlag10(int flag10) {
		this.flag10 = flag10;
	}
	public int getFlag11() {
		return flag11;
	}
	public void setFlag11(int flag11) {
		this.flag11 = flag11;
	}
	public int getFlag12() {
		return flag12;
	}
	public void setFlag12(int flag12) {
		this.flag12 = flag12;
	}
	public int getFlag13() {
		return flag13;
	}
	public void setFlag13(int flag13) {
		this.flag13 = flag13;
	}
	public int getFlag14() {
		return flag14;
	}
	public void setFlag14(int flag14) {
		this.flag14 = flag14;
	}
	public String getLink1() {
		return link1;
	}
	public void setLink1(String link1) {
		this.link1 = link1;
	}
	public String getLink2() {
		return link2;
	}
	public void setLink2(String link2) {
		this.link2 = link2;
	}
	public String getLink3() {
		return link3;
	}
	public void setLink3(String link3) {
		this.link3 = link3;
	}
	public String getLink4() {
		return link4;
	}
	public void setLink4(String link4) {
		this.link4 = link4;
	}
	public String getLink5() {
		return link5;
	}
	public void setLink5(String link5) {
		this.link5 = link5;
	}
	public String getLink6() {
		return link6;
	}
	public void setLink6(String link6) {
		this.link6 = link6;
	}
	public String getLink7() {
		return link7;
	}
	public void setLink7(String link7) {
		this.link7 = link7;
	}
	public String getLink8() {
		return link8;
	}
	public void setLink8(String link8) {
		this.link8 = link8;
	}
	public String getLink9() {
		return link9;
	}
	public void setLink9(String link9) {
		this.link9 = link9;
	}
	public String getLink10() {
		return link10;
	}
	public void setLink10(String link10) {
		this.link10 = link10;
	}
	public String getLink11() {
		return link11;
	}
	public void setLink11(String link11) {
		this.link11 = link11;
	}
	public String getLink12() {
		return link12;
	}
	public void setLink12(String link12) {
		this.link12 = link12;
	}
	
	
}
