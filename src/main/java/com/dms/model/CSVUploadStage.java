package com.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "csv_upload_stages")
public class CSVUploadStage {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cus_seq")
	@SequenceGenerator(name = "cus_seq", sequenceName = "cus_seq", allocationSize = 1)
	@Column(name = "cus_id")
	private Long cus_id;
	
	@Column(name="cus_cu_mid")
	private Long cus_cu_mid;
	
	@Column(name="cus_file_id")
	private Long cus_file_id;

	public Long getCus_id() {
		return cus_id;
	}

	public void setCus_id(Long cus_id) {
		this.cus_id = cus_id;
	}

	public Long getCus_cu_mid() {
		return cus_cu_mid;
	}

	public void setCus_cu_mid(Long cus_cu_mid) {
		this.cus_cu_mid = cus_cu_mid;
	}

	public Long getCus_file_id() {
		return cus_file_id;
	}

	public void setCus_file_id(Long cus_file_id) {
		this.cus_file_id = cus_file_id;
	}
}
