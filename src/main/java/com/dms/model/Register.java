package com.dms.model;

public class Register {
	
	private String type;
	private String username;
	private String name;
	private String email;
	private String gender;
	private String mobile;
	private String password;
	private String confirmPassword;
	private String rollNo;
	private Long enrollNo;
	private Integer enrollYear;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getRollNo() {
		return rollNo;
	}
	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}
	public Long getEnrollNo() {
		return enrollNo;
	}
	public void setEnrollNo(Long enrollNo) {
		this.enrollNo = enrollNo;
	}
	public Integer getEnrollYear() {
		return enrollYear;
	}
	public void setEnrollYear(Integer enrollYear) {
		this.enrollYear = enrollYear;
	}
	
	
}
