package com.dms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "csv_uploads")
public class CSVUpload {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "csv_seq")
	@SequenceGenerator(name = "csv_seq", sequenceName = "csv_seq", allocationSize = 1)
	@Column(name = "cu_id")
	private Long cu_id;
	
	@Column(name="cu_name")
	private String cu_name;
	
	@Column(name="cu_cr_by")
	private Long cu_cr_by;
	
	@Column(name="cu_cr_date")
	private Date cu_cr_date;
	
	@Column(name="cu_type")
	private String cu_type;

	public Long getCu_id() {
		return cu_id;
	}

	public void setCu_id(Long cu_id) {
		this.cu_id = cu_id;
	}

	public String getCu_name() {
		return cu_name;
	}

	public void setCu_name(String cu_name) {
		this.cu_name = cu_name;
	}

	public Long getCu_cr_by() {
		return cu_cr_by;
	}

	public void setCu_cr_by(Long cu_cr_by) {
		this.cu_cr_by = cu_cr_by;
	}

	public Date getCu_cr_date() {
		return cu_cr_date;
	}

	public void setCu_cr_date(Date cu_cr_date) {
		this.cu_cr_date = cu_cr_date;
	}

	public String getCu_type() {
		return cu_type;
	}

	public void setCu_type(String cu_type) {
		this.cu_type = cu_type;
	}
}
