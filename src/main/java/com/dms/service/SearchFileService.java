package com.dms.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.model.CaseFileDetail;

@Service
public class SearchFileService 
{
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public List<CaseFileDetail> getCaseFile(Long caseyear,Long casetype,String caseno) {
		List<CaseFileDetail> result=null;
	    String query=" SELECT cfd from CaseFileDetail cfd where cfd.fd_case_year="+caseyear+" and cfd.fd_case_type="+casetype+" and cfd.fd_case_no='"+caseno+"' ";
		result= em.createQuery(query).getResultList();
		
		return result;
	}

}
