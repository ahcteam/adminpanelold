package com.dms.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.model.ApplicationStage;
import com.dms.model.CaseFileStage;
import com.dms.model.CaveatFileStage;
import com.dms.model.PetitionerDetails;

@Service
public class CaseFileStageService {
	
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public CaseFileStage save(CaseFileStage s) {

		CaseFileStage master = null;
		try {
			master = em.merge(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	@Transactional
	public CaveatFileStage saveCaveat(CaveatFileStage caseStage) {
		// TODO Auto-generated method stub
		CaveatFileStage master = null;
		try {
			master = em.merge(caseStage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	@Transactional
	public ApplicationStage saveApplication(ApplicationStage cfs) {
		// TODO Auto-generated method stub
		ApplicationStage master = null;
		try {
			master = em.merge(cfs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return master;
	}
	
	@Transactional
	public CaseFileStage getByStage(Long rcd_id, Long lk_id) {
		// TODO Auto-generated method stub
		CaseFileStage result=new CaseFileStage();
		try{
		Query query=null;
		query = em.createQuery("SELECT c from CaseFileStage c where c.rcs_rcd_mid=:rcs_rcd_mid and c.rcs_stage_lid=:rcs_stage_lid order by c.rcs_id").setParameter("rcs_rcd_mid", rcd_id).setParameter("rcs_stage_lid", lk_id);
		result=(CaseFileStage) query.setMaxResults(1).getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	@Transactional
	public List<CaseFileStage> getStages(Long docId) {
		// TODO Auto-generated method stub
		List<CaseFileStage> result=null;
		Query query=null;
		query = em.createQuery("SELECT c from CaseFileStage c where c.rcs_rcd_mid=:id").setParameter("id", docId);
		result=query.getResultList();
		return result;
	}



} 
