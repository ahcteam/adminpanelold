package com.dms.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.model.CSVUpload;
import com.dms.model.CSVUploadStage;



@Service
public class CSVUploadService 
{
	@PersistenceContext
	private EntityManager em;
	

	@Transactional
    public CSVUpload save(CSVUpload c) {
    
		CSVUpload cupload = null;
    	try 
    	{	
    		cupload= em.merge(c);	    	
	    }catch (Exception e) {		
	    	e.printStackTrace();
		}
    	return cupload;
    }
	
	@Transactional
    public CSVUploadStage save(CSVUploadStage c) {
    
		CSVUploadStage cupload = null;
    	try 
    	{	
    		cupload= em.merge(c);	    	
	    }catch (Exception e) {		
	    	e.printStackTrace();
		}
    	return cupload;
    }
}
