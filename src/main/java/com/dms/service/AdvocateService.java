package com.dms.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.model.Advocate;
import com.dms.model.Register;

@Service
public class AdvocateService {

	@PersistenceContext
	private EntityManager em;
	
	private DataSource datasource;
	
	@Transactional
	public Advocate getByRollNo(Register register) {
		
		Advocate a= new Advocate();
		try {
			Query query  =  em.createQuery("SELECT a from Advocate a WHERE a.rollNo =:rollNo and a.enrollNo=:enrollNo and a.enrollYear=:enrollYear");
			query.setParameter("rollNo", register.getRollNo());
			query.setParameter("enrollNo", register.getEnrollNo());
			query.setParameter("enrollYear", register.getEnrollYear());
			a= (Advocate) query.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return a;
	}
}
