package com.dms.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dms.model.ApplicationStage;
import com.dms.model.CaseType;
import com.dms.model.Caveat;
import com.dms.model.CaveatCheckListMapping;
import com.dms.model.CaveatCourtFee;
import com.dms.model.CaveatDocuments;
import com.dms.model.CaveatPetitionerDetails;
import com.dms.model.CaveatRespondentDetails;
import com.dms.model.CaveatStage;
import com.dms.model.Document;
import com.dms.model.LowerCourtCaseType;
import com.dms.model.RegisteredCaseDetails;
import com.dms.model.State;
import com.dms.model.User;



@Service
public class CaveatService 
{
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public List<Caveat> getCaveatDetails(Long um_id) {
	List<Caveat> rcdDetails=null;
		rcdDetails= em.createQuery("SELECT cav FROM Caveat cav where cav.cav_cr_by ="+um_id+" order by cav.cav_id ").getResultList();
		return rcdDetails;
	}
	
	@Transactional
	public Caveat getRegisterCaveat(Long id) {
		Caveat result=null;
	    Query query=null;
		try {
			query = em.createQuery(" SELECT cav from Caveat cav where cav.cav_id=:id").setParameter("id", id);
			result=(Caveat) query.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Transactional
	public List<CaveatPetitionerDetails> getPetitioner(Long id) {
		
		List<CaveatPetitionerDetails> result=null;
		Query query=null;
		query = em.createQuery(" SELECT pt from CaveatPetitionerDetails pt where pt.cpt_rec_status=1 and pt.cpt_cav_mid=:id order by pt.cpt_sequence asc").setParameter("id", id);
		result=query.getResultList();
		return result;
	}


	@Transactional
	public List<CaveatRespondentDetails> getRespondent(Long id) {
		List<CaveatRespondentDetails> result=null;
		Query query=null;
		query = em.createQuery(" SELECT rt from CaveatRespondentDetails rt where rt.crt_rec_status=1 and rt.crt_cav_mid=:id order by rt.crt_sequence asc").setParameter("id", id);
		result=query.getResultList();
		return result;
	}
	
	
	@Transactional
    public Caveat save(Caveat c) {
    
		Caveat caveat = null;
    	try 
    	{	
    		caveat= em.merge(c);	    	
	    }catch (Exception e) {		
	    	e.printStackTrace();
		}
    	return caveat;
    }
	
	@Transactional
	public Caveat addCaveat(Caveat caveat) {
		Caveat cav=null;
		 try {
			
			int year = Calendar.getInstance().get(Calendar.YEAR);
			cav=em.merge(caveat);
			cav.setCav_draft_no(cav.getCav_id()+"_"+year);
			cav= em.merge(cav);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cav;
	}
	
	@Transactional
    public CaveatStage saveCaveatStage(CaveatStage cs) {
    
		CaveatStage caveatstage = null;
    	try 
    	{	
    		caveatstage= em.merge(cs);	    	
	    }catch (Exception e) {		
	    	e.printStackTrace();
		}
    	return caveatstage;
    }
	
	@Transactional
    public CaveatDocuments saveCaveatDocument(CaveatDocuments cs) {
    
		CaveatDocuments caveatdocument = null;
    	try 
    	{	
    		caveatdocument= em.merge(cs);	    	
	    }catch (Exception e) {		
	    	e.printStackTrace();
		}
    	return caveatdocument;
    }
	
	@Transactional
	public Long getCPSequenceCount(Long id) {

		Long result=0L;
		 Query query=null;
		try{
			query = em.createQuery("SELECT count(pt.cpt_id) FROM CaveatPetitionerDetails pt where pt.cpt_cav_mid=:id").setParameter("id", id);
		   result= (Long) query.getSingleResult();
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	 @Transactional
		public CaveatPetitionerDetails addPetitioner(CaveatPetitionerDetails pDetails) {
	    	
		 CaveatPetitionerDetails pd=null;
	    	 try {
	    		
				pd=em.merge(pDetails);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return pd;
		}
	 
	 @Transactional
	public CaveatPetitionerDetails deletePetitioner(User user, Long id) {
			CaveatPetitionerDetails oldPetitioner=null;
			CaveatPetitionerDetails pDetails=null;
		    user.setMod_by(id);
		    user.setMod_date(new Date());
		    oldPetitioner=em.find(CaveatPetitionerDetails.class, id);
		    oldPetitioner.setCpt_rec_status(2);
		    pDetails =em.merge(oldPetitioner);
			
			return pDetails;
		}
	 
	 @Transactional
	 public Long getCRSeqCount(Long id) {

	 	Long result=0L;
	 	
	 	try{
	 		result = (Long) em.createQuery("SELECT count(rt.crt_id) FROM CaveatRespondentDetails rt where rt.crt_cav_mid=:id").setParameter("id", id).getSingleResult();
	 	}catch(Exception e)	{
	 		e.printStackTrace();
	 	}
	 	return result;
	 }
	 
	 @Transactional
	 public CaveatRespondentDetails addRespondent(CaveatRespondentDetails rDetails) {
	 	CaveatRespondentDetails rd=null;
	 	 try {
	 		
	 		rd=em.merge(rDetails);
	 	} catch (Exception e) {
	 		// TODO Auto-generated catch block
	 		e.printStackTrace();
	 	}
	 	
	 	return rd;
	 }
	 @Transactional
	 public CaveatRespondentDetails deleteRespondent(Long user, Long id) {
	 	CaveatRespondentDetails rDetails=null;
	 	rDetails=em.find(CaveatRespondentDetails.class, id);
	 	rDetails.setCrt_rec_status(2);
	    rDetails =em.merge(rDetails);
	 	
	 	return rDetails;
	 }
	 
	 @Transactional
	 public CaveatCourtFee addCourtFee(CaveatCourtFee courtFee) {
	 	CaveatCourtFee result=null;
	     
	 	  result=em.merge(courtFee);
	 	
	 	return result;
	 }
	 
	 @Transactional
		public List<CaveatCourtFee> getCaveatCourtFee(Long id) {
			
			List<CaveatCourtFee> result=null;
			Query query=null;
			query = em.createQuery(" SELECT cf from CaveatCourtFee cf where cf.ccf_rec_status=1 and cf.ccf_cav_mid=:id").setParameter("id", id);
			result=query.getResultList();
			return result;
		}
	 
	 @Transactional
	 public List<CaveatDocuments> getUploadedCaveates(Long cav_id) 
	 {
			List<CaveatDocuments> result=new ArrayList<CaveatDocuments>();
			
			try{
				result = em.createQuery("SELECT cd FROM CaveatDocuments cd  where cd.cd_cav_mid="+cav_id+" and cd.cd_rec_status=1 ").getResultList();
			}catch(Exception e)	{
				e.printStackTrace();
			}
			return result;
	}
	 
	 @Transactional
		public Caveat getByPk(Long id) {
		 Caveat result =null;
		 try {
			result = (Caveat) em.createQuery("select c from Caveat c where c.cav_id = :cav_id").setParameter("cav_id", id).getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return result;
		}
	 
	 	@Transactional
			public CaveatPetitionerDetails getFirstPetitioner(Long id) {
			 CaveatPetitionerDetails result=new CaveatPetitionerDetails();
				try{
				Query query=null;
				query = em.createQuery("SELECT pt from CaveatPetitionerDetails pt where pt.cpt_rec_status=1 and pt.cpt_cav_mid=:id order by cpt_id asc").setParameter("id", id);
				result=(CaveatPetitionerDetails) query.setMaxResults(1).getSingleResult();
				}catch(Exception e){
					e.printStackTrace();
				}
				return result;
			}
		 
		    @Transactional
			public CaveatRespondentDetails getFirstRespondent(Long id) {
		    	   CaveatRespondentDetails result=new CaveatRespondentDetails();
				try{
				Query query=null;
				query = em.createQuery(" SELECT rt from CaveatRespondentDetails rt where rt.crt_rec_status=1 and rt.crt_cav_mid=:id order by crt_id asc").setParameter("id", id);
				result=(CaveatRespondentDetails) query.setMaxResults(1).getSingleResult();
				}catch(Exception e){
					e.printStackTrace();
				}
		           return result;
		    }
		    
	    @Transactional
		public List<CaveatCourtFee> getCourtFee(Long id) {
			
			List<CaveatCourtFee> result=null;
			Query query=null;
			query = em.createQuery(" SELECT cf from CaveatCourtFee cf where cf.ccf_rec_status=1 and cf.ccf_cav_mid=:id").setParameter("id", id);
			result=query.getResultList();
			return result;
		}
	    
	    @Transactional
		public Long  getDiarySequence() {
			
			Long  sequence = 0L;
			
			String query = "select nextval('diary_sequence')";
			
			try {
				sequence= ((BigInteger) em.createNativeQuery(query).getResultList().get(0)).longValue();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			return sequence;
		}

		public List<Caveat> getDraftDetails(Long um_id) {
			// TODO Auto-generated method stub
			List<Caveat> cavDetails=null;
			cavDetails= em.createQuery("SELECT cav FROM Caveat cav where cav.cav_assign_to ="+um_id+" order by cav.cav_id ").getResultList();
			return cavDetails;
		}
		
		@Transactional
		public CaveatDocuments getCaveatDocuments(Long cd_id) 
		{
			CaveatDocuments result=new CaveatDocuments();
			
			try{
				result = (CaveatDocuments) em.createQuery("SELECT cd FROM CaveatDocuments cd  where cd.cd_id="+cd_id).getSingleResult();
			}catch(Exception e)	{
				e.printStackTrace();
			}
			return result;
		}
		
		@Transactional
		 public boolean deleteCaveatDocuments(Long id) 
		 {
			 boolean flag=false;
			 	CaveatDocuments oldDocument=null;
			 	oldDocument=em.find(CaveatDocuments.class, id);
			    em.remove(oldDocument);
			    flag= true;
			    return flag;
		 }
		
		public List<CaveatCheckListMapping> getCaveatCheckList(Long doc) {
			// TODO Auto-generated method stub
			List<CaveatCheckListMapping> result=null;
			Query query=null;
			query = em.createQuery("SELECT c from CaveatCheckListMapping c where c.cm_rec_status=1 and c.cm_cav_mid=:id").setParameter("id", doc);
			result=query.getResultList();
			return result;
		}
		
		@Transactional
		public List<Caveat> searchCaveatFiles(Long stageId, String stageDate) {
			// TODO Auto-generated method stub
			List<Caveat> cav=new ArrayList<Caveat>();
			if(stageDate!=null)
			{
				try {
					cav=em.createQuery("SELECT c FROM Caveat c, CaveatStage cs where c.cav_id=cs.cs_cav_mid and c.cav_stage_lid="+stageId+" and to_date(to_char(cs.cs_cr_date,'yyyy-mm-dd'),'yyyy-mm-dd') = to_date('"+stageDate+"','yyyy-mm-dd') and cs.cs_stage_lid="+stageId+" order by cs.cs_id").getResultList();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else
			{
				try {
					cav= em.createQuery("SELECT c FROM Caveat c where c.cav_stage_lid=:stageId order by c.cav_id ").setParameter("stageId",stageId).getResultList();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return cav;
		}
		
		@Transactional
		public List<Caveat> searchCaveatFilesForCSV(Long stageId) {
			// TODO Auto-generated method stub
			List<Caveat> cav=new ArrayList<Caveat>();
				try {
					cav= em.createQuery("SELECT c FROM Caveat c,CaveatStage cs where c.cav_id=cs.cs_cav_mid and c.cav_stage_lid=:stageId and cs.cs_stage_lid=:stageId order by cs.cs_cr_date ").setParameter("stageId",stageId).getResultList();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return cav;
		}
		
		@Transactional
		public CaseType getCaseTypeById(Long id) {
			
			CaseType r= new CaseType();
			try {
				Query query  =  em.createQuery("SELECT r from CaseType r WHERE r.ct_id =:id");
				query.setParameter("id", id);
				r= (CaseType) query.getSingleResult();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return r;
		}
		@Transactional
		public LowerCourtCaseType getLCCaseTypeById(Long id) {
			
			LowerCourtCaseType r= new LowerCourtCaseType();
			try {
				Query query  =  em.createQuery("SELECT r from LowerCourtCaseType r WHERE r.ct_id =:id");
				query.setParameter("id", id);
				r= (LowerCourtCaseType) query.getSingleResult();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return r;
		}
		@Transactional
		public CaveatStage getByStage(Long cav_id, Long lk_id) {
			// TODO Auto-generated method stub
			CaveatStage result=new CaveatStage();
			try{
			Query query=null;
			query = em.createQuery("SELECT c from CaveatStage c where c.cs_cav_mid=:cs_cav_mid and c.cs_stage_lid=:cs_stage_lid order by c.cs_id").setParameter("cs_cav_mid", cav_id).setParameter("cs_stage_lid", lk_id);
			result=(CaveatStage) query.setMaxResults(1).getSingleResult();
			}catch(Exception e){
				e.printStackTrace();
			}
			return result;
		}
		
		@Transactional
		public State getStateById(Long cav_state_mid) {
			// TODO Auto-generated method stub
			State result=new State();
			try{
			Query query=null;
			query = em.createQuery("SELECT s from State s where s.s_id=:cav_state_mid").setParameter("cav_state_mid", cav_state_mid);
			result=(State) query.getSingleResult();
			}catch(Exception e){
				e.printStackTrace();
			}
			return result;
		}
		@Transactional
		public Caveat getByDiaryNo(String diaryNo) {

			Caveat result=null;
		    Query query=null;
			query = em.createQuery("SELECT c from Caveat c where c.cav_diary_no=:cav_diary_no").setParameter("cav_diary_no", diaryNo);
			result=(Caveat) query.getSingleResult();
			
			return result;
		}
		public List<CaveatCheckListMapping> getCaveatReportingHistory(Long doc) {
			// TODO Auto-generated method stub
			List<CaveatCheckListMapping> result=null;
			Query query=null;
			query = em.createQuery("SELECT c from CaveatCheckListMapping c where c.cm_cav_mid=:id").setParameter("id", doc);
			result=query.getResultList();
			return result;
		}

		public List<CaveatStage> getStages(Long docId) {
			// TODO Auto-generated method stub
			List<CaveatStage> result=null;
			Query query=null;
			query = em.createQuery("SELECT a from CaveatStage a where a.cs_cav_mid=:id").setParameter("id", docId);
			result=query.getResultList();
			return result;
		}
		
		public CaveatStage getCaveatStage(Long id,Long stageLid) {
			CaveatStage result=null;
		    Query query=null;
			try {
				query = em.createQuery("SELECT cav from CaveatStage cav where cav.cs_cav_mid=:id and cav.cs_stage_lid=:cs_stage_lid").setParameter("id", id).setParameter("cs_stage_lid", stageLid);
				result=(CaveatStage) query.getSingleResult();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return result;
		}
}
