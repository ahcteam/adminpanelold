

package com.dms.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bouncycastle.crypto.macs.CMac;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dms.model.ActionResponse;
import com.dms.model.Lookup;
import com.dms.model.SecurityQuestion;
import com.dms.model.User;
import com.dms.service.LookupService;
import com.dms.service.SecurityQuestionService;
import com.dms.utility.GlobalFunction;


@Component
public class SecurityQuestionValidator {
	
	@Autowired
	SecurityQuestionService securityQuestionService;
	
	@Autowired
	LookupService lookupservice;
	
	GlobalFunction cm = new GlobalFunction();

public ActionResponse doValidation(SecurityQuestion sq){
		
		ActionResponse response = new ActionResponse();
		Validator validation=new Validator();
	
		List<String> errorList = new ArrayList<String>();
		Map<String, List> error = new HashMap<String, List>();
		String status = "TRUE";
		
		if (sq.getPra_answer().equals(""))
			sq.setPra_answer(null);
		validation.isRequired("Answer",sq.getPra_answer());
		validation.isRequired("New Password",sq.getPassword());
		
		
		if(sq.getPassword()!=null && sq.getPasswordc()!=null)
			validation.isMatch("Password",sq.getPassword(),sq.getPasswordc());
		      
			
		error=validation.getError();
		
		if(sq.getPassword()!=null)
		{
			Lookup lk =new Lookup();
			List<Lookup> lkup=lookupservice.CheckRegex("REGEX_COMPLEXITY");
			String regex=lkup.get(0).getLk_longname();
			String msg=lkup.get(0).getLk_value();
			validation.checkRegEx("password", sq.getPassword(),regex, msg);
		}
		
		if(!error.isEmpty())
		{
			status = "FALSE";
		}
				
		response.setResponse(status);
		response.setDataMapList(error);
		
		
		return response;
	}
public ActionResponse doValidationForChanePwd(User u,String oldpasswd){
	
	ActionResponse response = new ActionResponse();
	Validator validation=new Validator();

	List<String> errorList = new ArrayList<String>();
	Map<String, List> error = new HashMap<String, List>();
	String status = "TRUE";
	
	validation.isRequired("Old Password",u.getPassword());
	//validation.isRequired("New Password",u.getPasswordc());
	
	String encryptepswd = cm.md5encryption(u.getPassword());
	
	//if(u.getPassword()!="" && sq.getPasswordc()!="")
		validation.isMatch("Password",oldpasswd,encryptepswd);
	      
	if(u.getPasswordn()!="")
	{
		Lookup lk =new Lookup();
		List<Lookup> lkup=lookupservice.CheckRegex("REGEX_COMPLEXITY");
		String regex=lkup.get(0).getLk_longname();
		String msg=lkup.get(0).getLk_value();
		validation.checkRegEx("password", u.getPasswordn(),regex, msg);
	}
	
	error=validation.getError();
	
	
	
	
	if(!error.isEmpty())
	{
		status = "FALSE";
	}
			
	response.setResponse(status);
	response.setDataMapList(error);
	
	
	return response;
}
}