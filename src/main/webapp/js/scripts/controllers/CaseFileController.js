var EDMSApp = angular.module("EDMSApp", ['ngFileUpload','ngMask','ui.bootstrap']);
EDMSApp.directive('loading', ['$http', function ($http) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    elm.show();
                } else {
                    elm.hide();
                }
            });
        }
    };
}]);



EDMSApp.controller('CaseFileController',['$scope','$http','Upload',function ($scope, $http,Upload) {
	  var urlBase="/adminpanel/";
	  $scope.picFile='';
	  $scope.stages=[];
	  $scope.search={};
	  $scope.registerFile={};
	  $scope.toggleMax = function() {
		    //$scope.minDate = $scope.minDate ? null : new Date();
			$scope.maxDate = new Date();
		};
		$scope.toggleMax();
		
		$scope.open = function($event,type) {
		    $event.preventDefault();
		    $event.stopPropagation();
		    
		    if(type=="stageDate")
		    	$scope.stageDate= true;
		    
		};
		
		$scope.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1		    
		};
		
		$scope.formats = ['dd-MMMM-yyyy','dd-mm-yyyy', 'yyyy/MM/dd', 'dd-MM-yyyy', 'shortDate'];
		$scope.format = $scope.formats[3];
	  
	  getStages();
	  function getStages(){
		  $http.get(urlBase+'master/getstages').success(function (data) {
		    		$scope.stages=data.modelList;		    	
		    	
		      }).
		      error(function(data, status, headers, config) {
		      	console.log("Error in getting tree data");
		      });
	  }
	  $scope.downloadCaseFiles=function(){
		  window.open(urlBase+"casefile/downloadCSV","_blank");
	  }
	  
	  $scope.searchCaseFiles=function() 
	  {
		  $http.post(urlBase+'casefile/getCaseFileList',$scope.search).success(function (data) {
		    	if(data.response=="TRUE")
		    		$scope.caseFileList=data.modelList;		    	
		    	else
		    		$scope.caseFileList=[];
		      }).
		      error(function(data, status, headers, config) {
		      	console.log("Error in getting tree data");
		      });
	  }	  
	  $scope.save=function() 
		{
			  var file=$scope.picFile;
			  
			    file.upload = Upload.upload({
			      url: urlBase + 'casefile/upload',
			      headers: {
			    	  'optional-header': 'header-value'
			        },
	    		   file:file,
			    });

			    file.upload.then(function (response) {
			        if(response.data.response=="TRUE"){
			        	$scope.errorlist =null;
			        	alert(response.data.data);
			        	$("#uploadCSV").modal("hide");
			        	//window.location.reload();
			        }else{
			        	$scope.errorlist = response.data.dataMapList;
			        }
			      }, function (response) {
			        
			      }, function (evt) {
			        // Math.min is to fix IE which reports 200% sometimes
			        //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			      });

			      file.upload.xhr(function (xhr) {
			        // xhr.upload.addEventListener('abort', function(){console.log('abort complete')}, false);
			      });
			}  
	  
	  $scope.getReporting=function(casefile){
		  	$http.get(urlBase+ 'scrutiny/getCaseHistory', {
					params : {
						'docId' : casefile.rcd_id
					}
				}).success(function(data, status, headers, config) {	            
				          $scope.checkListHistory = data.modelList;	            
				}).error(function(data, status, headers, config) {
				});			
	  }
	  $scope.getStages=function(casefile){
		  $scope.casefile=casefile;
		  	$http.get(urlBase+ 'casefile/getCaseStages', {
					params : {
						'docId' : casefile.rcd_id
					}
				}).success(function(data, status, headers, config) {	            
				          $scope.caseHistory = data.modelList;	            
				}).error(function(data, status, headers, config) {
				});			
	  }
	  
	  $scope.showDocument=function(selectedfile){
			var response = $http.get(urlBase+'casefile/copyFile',{params: {'rcd_id': selectedfile.rcd_id}});
			response.success(function(data, status, headers, config) {		
				console.log(data);
				if(data.response == "TRUE")
				{
					window.open(urlBase+"/uploads/"+data.data,'_blank');
				}
				else if(data.response == "FALSE")
				{
					alert(data.data);
				}
			});
			response.error(function(data, status, headers, config) {
				bootbox.alert("Error");
			});
		};
		
		$scope.getReport=function(casefile){
			$scope.registerFile=angular.copy(casefile);
			$scope.srd={};
			$scope.caveat ={};
	        $scope.courtFee=null;
		  	$http.get(urlBase+ 'casefile/getReport', {
					params : {
						'docId' : casefile.rcd_id
					}
				}).success(function(data) {	            
				          $scope.srd = data.srd;
				          $scope.caveat = data.caveat;
				          $scope.courtFee=data.courtFee;
				}).error(function(data) {
				});			
	  }
		
		document.getElementById("btnPrint").onclick = function() 
		{
			var printContents = document.getElementById("report_table").innerHTML;
		    var popupWin = window.open('', '_blank', 'width=300,height=300');
		    popupWin.document.open();
		    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
		    popupWin.document.close();
		}

		function printElement(elem, append, delimiter) 
		{
		  	var domClone = elem.cloneNode(true);
		    var $printSection = document.getElementById("printSection");
		  
		    if (!$printSection) 
		    {
		        var $printSection = document.createElement("div");
		        $printSection.id = "printSection";
		        document.body.appendChild($printSection);		        
		    }

		    if (append !== true) 
		    {
		        $printSection.innerHTML = "";
		    }

		    else if (append === true)
		    {
		        if (typeof(delimiter) === "string") {
		            $printSection.innerHTML += delimiter;
		        }
		        else if (typeof(delimiter) === "object") {
		            $printSection.appendChlid(delimiter);
		        }
		    }

		    $printSection.appendChild(domClone);		   
		}
	  
}]);