var EDMSApp = angular.module('EDMSApp', ['ngFileUpload','ngMask','ui.bootstrap']);
EDMSApp.directive('loading', ['$http', function ($http) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    elm.show();
                } else {
                    elm.hide();
                }
            });
        }
    };
}]);



EDMSApp.controller('ApplicationController',['$scope','$http','Upload',function ($scope, $http,Upload) {
	  var urlBase="/adminpanel/";
	  $scope.search={};
	  $scope.toggleMax = function() {
		    //$scope.minDate = $scope.minDate ? null : new Date();
			$scope.maxDate = new Date();
		};
		$scope.toggleMax();
		
		$scope.open = function($event,type) {
		    $event.preventDefault();
		    $event.stopPropagation();
		    
		    if(type=="stageDate")
		    	$scope.stageDate= true;
		    
		};
		
		$scope.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1		    
		};
		
		$scope.formats = ['dd-MMMM-yyyy','dd-mm-yyyy', 'yyyy/MM/dd', 'dd-MM-yyyy', 'shortDate'];
		$scope.format = $scope.formats[3];
	  
		getStages();
	  function getStages(){
		  $http.get(urlBase+'master/getstages').success(function (data) {
		    		$scope.stages=data.modelList;		    	
		    	
		      }).
		      error(function(data, status, headers, config) {
		      	console.log("Error in getting tree data");
		      });
	  }
	  $scope.downloadCaseFiles=function(){
		  window.open(urlBase+"application/downloadCSV","_blank");
	  } 
	  $scope.searchApplications=function() 
	  {
		  $http.post(urlBase+'application/getApplicationList',$scope.search).success(function (data) {
		    	if(data.response=="TRUE")
		    		$scope.applicationList=data.modelList;
		    	else
		    		$scope.applicationList=[];
		    		
		      }).
		      error(function(data, status, headers, config) {
		      	console.log("Error in getting tree data");
		      });
	  }	
	  $scope.save=function() 
		{
			  var file=$scope.picFile;
			  
			    file.upload = Upload.upload({
			      url: urlBase + 'application/upload',
			      headers: {
			    	  'optional-header': 'header-value'
			        },
	    		   file:file,
			    });

			    file.upload.then(function (response) {
			        if(response.data.response=="TRUE"){
			        	$scope.errorlist =null;
			        	alert(response.data.data);
			        	$("#uploadCSV").modal("hide");
			        	//window.location.reload();
			        }else{
			        	$scope.errorlist = response.data.dataMapList;
			        }
			      }, function (response) {
			        
			      }, function (evt) {
			        // Math.min is to fix IE which reports 200% sometimes
			        //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			      });

			      file.upload.xhr(function (xhr) {
			        // xhr.upload.addEventListener('abort', function(){console.log('abort complete')}, false);
			      });
			}
	  $scope.getReporting=function(application){
		  
		  	$http.get(urlBase+ 'scrutiny/getApplicationHistory', {
					params : {
						'docId' : application.ap_id
					}
				}).success(function(data, status, headers, config) {	            
				          $scope.checkListHistory = data.modelList;	            
				}).error(function(data, status, headers, config) {
				});			
	  }
	  $scope.getStages=function(application){
		  $scope.application=application;
		  	$http.get(urlBase+ 'application/getApplicationStages', {
					params : {
						'docId' : application.ap_id
					}
				}).success(function(data, status, headers, config) {	            
				          $scope.applicationHistory = data.modelList;	            
				}).error(function(data, status, headers, config) {
				});			
	  }
	  
	  $scope.showDocument=function(selectedfile){
			var response = $http.get(urlBase+'application/copyApplicationFile',{params: {'ap_id': selectedfile.ap_id}});
			response.success(function(data, status, headers, config) {		
				console.log(data);
				if(data.response == "TRUE")
				{
					window.open(urlBase+"/uploads/"+data.data,'_blank');
				}
				else if(data.response == "FALSE")
				{
					alert(data.data);
				}
			});
			response.error(function(data, status, headers, config) {
				bootbox.alert("Error");
			});
		};
}]);