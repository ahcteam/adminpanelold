var EDMSApp = angular.module("EDMSApp", ['ngMask','ui.bootstrap']);

EDMSApp.controller("editProfileCtrl", function($scope, $http, $window) {

	$scope.error={};
	$scope.userobj = {};
	$scope.errors = [];
	$scope.date={};
	$scope.validitydate={};
	var baseUrl = "/adminpanel/";

	$scope.error = "";
	$scope.password="";

	//$('#changePassword_Modal').modal('show');

	function convertDate(inputFormat) 
	{
		  function pad(s) { return (s < 10) ? '0' + s : s; }
		  var d = new Date(inputFormat);
		  return [ d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-');
	}
	
	
       getEditProfileData();
	
	  function getEditProfileData() 
	  {
		//alert(11);
		//console.log("Edit Profile");
		$http.get(baseUrl+'user/getUserDetails').success(function(data) {
			console.log(data);
			$scope.date = data.data.um_pass_validity_date;
			//alert($scope.date);
			if($scope.date!=null){
				$scope.validitydate=convertDate($scope.date);
				//alert($scope.validitydate);
			}
	            $scope.userobj = data.data;
	            
			console.log($scope.userobj);
		}).error(function(data, status, headers, config) {
			console.log("Error in getting property details");
		});

	};
	
	
	

	$scope.savePassword = function(data) {
		console.log(data);
		console.log("********");

		$scope.password;

		$scope.entity = {
			'password' : $scope.password
		}

		//console.log($scope.password);

		var response = $http.post(baseUrl + 'user/updateUserDetails',
				$scope.entity);
		response.success(function(data, status, headers, config) {
			if (data.response == "TRUE") {
				alert("Password Change Successfully!");
				//window.location.href = baseUrl + "views/landingPage";
				$scope.errorlist = [];
				$scope.entity = "";
				$window.location.href = baseUrl;
				$scope.errorlist = [];

			} else {

				$scope.errorlist = data.dataMapList;

			}

		});
		response.error(function(data, status, headers, config) {
			console.log("Error in getting Master");
		});

	}
	
	
	$scope.changePassword = function(data) {
		console.log(data);
		

		$('#changePassword_Modal').modal({
		    backdrop: 'static',
		    keyboard: false
		});		
	/*	$scope.passwordn;
		$scope.passwordcn;*/
		
		if($scope.passwordn == $scope.passwordcn ){
			
			$scope.error= "";
	
			$scope.entity={'password':$scope.password,'passwordn':$scope.passwordn}
		
		    var response = $http.post(baseUrl+ 'user/changePass',$scope.entity);
		    response.success(function(data, status, headers, config) {
			if (data.response == "TRUE") {
				alert("Password changed Successfully!");
				//window.location.href = "/pdms/pdms/logout";
				//window.location.href=baseUrl+"views/landingPage";
				$('#changePassword_Modal').modal('hide');
				$window.location.href = baseUrl;
				$scope.errorlist = [];
			} else 
			{ 
				$scope.errorlist = data.dataMapList;
         	    //bootbox.alert("Password and Confirm Password not matched");
				$scope.ansDetails = [];

			}

		});
		response.error(function(data, status, headers, config) {
			console.log("Error in getting Master");
		});

			
			
		}else{
			//alert("New password and confirm passworddidn't matched");
			$scope.error="New password and confirm passworddidn't matched";
				
   		        	
		}
		
		
			
	}
	
	
	
	
	
	
	
	$scope.save = function(userobj) 
	 { 
	   $scope.entity = userobj;

		var response = $http.post(baseUrl+'user/updateUser',$scope.entity);
		response.success(function(data, status, headers, config) {
			$scope.errorlist = {};
			if (data.response == "FALSE") {
				$scope.errorlist = data.dataMapList;
			}
			else 
			{
				 alert("User updated successfully");
		  }

		});
	};
	
	/*$scope.resetModel = function(userobj) 
	 { 
	   $scope.entity = userobj;

		var response = $http.post(baseUrl+'user/updateUser',$scope.entity);
		response.success(function(data, status, headers, config) {
			$scope.errorlist = {};
			if (data.response == "FALSE") {
				$scope.errorlist = data.dataMapList;
			}
			else 
			{
				 alert("User updated successfully");
		  }

		});
	};*/
	
	$scope.openModel = function() 
	 {
		//$('#changePassword_Modal').modal('show');
		$('#changePassword_Modal').modal({
		    backdrop: 'static',
		    keyboard: false
		});
		
	 };
	$scope.resetform = function()
	{
		$scope.passwordn="";
		$scope.passwordcn="";
		$scope.password="";
		$scope.errorlist="";
		$scope.error="";
		//$scope.changePasswordForm.$setUntouched();
		changePasswordForm($scope);
		
	}
	

	function changePasswordForm($scope){
		$scope.changePasswordForm.$setUntouched();
		$scope.changePasswordForm.$setPristine();
	}
	
	/*
	 $scope.landingForm= function() 
	   {
              window.location.href=baseUrl+"views/landingPage";
	   } */        	
  
});
