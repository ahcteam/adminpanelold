var EDMSApp = angular.module('EDMSApp', []);
EDMSApp.directive('loading', ['$http', function ($http) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    elm.show();
                } else {
                    elm.hide();
                }
            });
        }
    };
}]);





EDMSApp.controller('ScrutinyCtrl',['$scope','$http',function ($scope, $http) {
 
 
	var urlBase="/adminpanel/";
  
  	$scope.getCaseList=function(){
  		$http.get(urlBase+'scrutiny/getCaseList').success(function (data) {
    	$scope.count=data.data;
      	$scope.draftList=data.modelList;
    	  
      }).
      error(function(data, status, headers, config) {
      	console.log("Error in getting tree data");
      });
	};
	$scope.getRejectedCaseList=function(){
  		$http.get(urlBase+'scrutiny/getRejectedCaseList').success(function (data) {
    	$scope.draftRejectedList=data.modelList;    	  
      }).
      error(function(data, status, headers, config) {
      	console.log("Error in getting tree data");
      });
	};
  	$scope.previewCaseDetails=function(id){
		window.open(urlBase+"scrutiny/case/"+id,"_self");
	}
  	$scope.getCaveatList=function(){
	  	$http.get(urlBase+'scrutiny/getCaveatList').success(function (data) {
    	$scope.count=data.data;
      	$scope.draftList=data.modelList;    	  
      }).
      error(function(data, status, headers, config) {
      	console.log("Error in getting tree data");
      });
	}
  	$scope.getRejectedCaveats=function(){
  		$http.get(urlBase+'scrutiny/getRejectedCaveats').success(function (data) {
    	$scope.draftRejectedList=data.modelList;    	  
      }).
      error(function(data, status, headers, config) {
      	console.log("Error in getting tree data");
      });
	};
  	$scope.previewCaveatDetails=function(id){
		window.open(urlBase+"scrutiny/caveat/"+id,"_self");
	}
  	$scope.getApplicationList=function(){
	  	$http.get(urlBase+'scrutiny/getApplicationList').success(function (data) {
    	$scope.count=data.data;
      	$scope.draftList=data.modelList;
    	  
      }).
      error(function(data, status, headers, config) {
      	console.log("Error in getting tree data");
      });
	}
  	$scope.getRejectedApplications=function(){
  		$http.get(urlBase+'scrutiny/getRejectedApplications').success(function (data) {
    	$scope.draftRejectedList=data.modelList;    	  
      }).
      error(function(data, status, headers, config) {
      	console.log("Error in getting tree data");
      });
	};
  	$scope.previewApplicationDetails=function(id){
		window.open(urlBase+"scrutiny/application/"+id,"_self");
	}	
  	$scope.updateCaseStatus=function(rcd_id,status){
  		var confirmbox = confirm("Do you really want to submit this file");
  		if (confirmbox) 
  		{
  			$scope.entity={'rcd_id':rcd_id,'status':status}
  			var response = $http.post(urlBase+ 'supervisor/submit_case_file',$scope.entity);
			response.success(function(data, status, headers, config) {
				window.location.reload();
			});
			response.error(function(data, status, headers, config) {
				alert("Error");
			});
  		}
  	};
  	$scope.updateCaveatStatus=function(cav_id,status){
  		var confirmbox = confirm("Do you really want to submit this file");
  		if (confirmbox) 
  		{
  			$scope.entity={'cav_id':cav_id,'status':status}
  			var response = $http.post(urlBase+ 'supervisor/submit_caveat_file',$scope.entity);
			response.success(function(data, status, headers, config) {
				window.location.reload();
			});
			response.error(function(data, status, headers, config) {
				alert("Error");
			});
  		}
  	};
  	$scope.updateApplicationStatus=function(ap_id,status){
  		var confirmbox = confirm("Do you really want to submit this file");
  		if (confirmbox) 
  		{
  			$scope.entity={'ap_id':ap_id,'status':status}
  			var response = $http.post(urlBase+ 'supervisor/submit_application_file',$scope.entity);
			response.success(function(data, status, headers, config) {
				window.location.reload();
			});
			response.error(function(data, status, headers, config) {
				alert("Error");
			});
  		}
  	};
}]);