var edmsApp = angular.module("EDMSApp", []);
edmsApp.controller("OLReportController",['$scope','$http', function($scope,$http) {
	var urlBase="/adminpanel/";
	$scope.model={};
	  
	$scope.getList=function(){
		getData();
	}
	function getData(){	
		$http.get(urlBase+'olreport/getall')
			.success(function(data) {
				$scope.olreports=data.modelList;
		}).error(function(data, status, headers, config) {
			console.log("Error in getting Cause List Data ");
		});	
	};
		$scope.onFileSelect = function ($files) {
        $scope.uploadProgress = 0;
        $scope.selectedFile = $files;
    };

	$scope.viewFile=function(id){
		  window.open(urlBase+"olreport/viewdocument/"+id,"_blank");
	  }
		
}]);
