<jsp:include page="../content/header2.jsp"></jsp:include>
<%@ page import="com.dms.model.User"%>	

<% 
User user = null;
if(session.getAttribute("USER")!=null)
	 user = (User)session.getAttribute("USER");

String role=user.getUserroles().get(0).getLk().getLk_longname();

%>
	<div id="content" class="content">
			<div class="container-fluid" ng-controller="ScrutinyCtrl" >
			
			
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			    <ul class="nav nav-tabs">
					<li class="active"><a href="#nav-tab-1"  id ="1" data-toggle="tab" >Fresh Cases</a></li>
					<% if(role.equals("CaseScrutiny")){ %>
					<li class=""><a href="#nav-tab-2" id="2" data-toggle="tab">Rejected Case</a></li>
					<% } %>	
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active in" id="nav-tab-1">
						<jsp:include page="newcases.jsp"></jsp:include>
					</div>
					<% if(role.equals("CaseScrutiny")){ %>
           			<div class="tab-pane fade" id="nav-tab-2" >
                  		<jsp:include page="rejectedcases.jsp"></jsp:include>
					</div>
					<% } %>	
				</div>
			    
			        <!-- begin panel -->
                    
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
			
			</div>
	</div>
	
	</div>
	
	</body>
	<% //if(role.equals("Advocate") || role.equals("InPerson")){ %>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/scripts/controllers/ScrutinyController.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/apps.min.js"></script>
	<script>
		$(document).ready(function() {
			App.init();
			
		});
	</script>
</html>