<div class="panel panel-inverse">
                        
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-striped table-bordered" ng-init="getRejectedCaseList()" >
                                    <thead>
                                        <tr>
 	                                        <th>Draft No</th>
                                            <th>Diary No</th>
                                            <th>First Petitioner</th>
                                            <th>First Respondent</th>
                                            <th>Case Type</th>
                                            <th>Submit Timing</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr  ng-repeat="row in draftRejectedList" class="odd gradeX">
                                        	 <td>{{row[0]}}</td>
                                             <td>{{row[1]}}</td>
                                             <td>{{row[2]}}</td>
                                             <td>{{row[3]}}</td>                                          
                                             <td>{{row[4]}}</td>
                                             <td>{{row[5] | date:"dd/MM/yyyy HH:mm:ss"}}</td>
                                             <td><button class="btn btn-success" ng-click="previewCaseDetails(row[6])">Preview</button>
                                             
                                             </td>
                                          </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>