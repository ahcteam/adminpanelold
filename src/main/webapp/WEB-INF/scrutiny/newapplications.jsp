<div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>                                
                            </div>
                            <h4 class="panel-title">View Applications</h4>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-striped table-bordered" ng-init="getApplicationList()">
                                    <thead>
                                        <tr>
                                            <th>Diary No</th>
                                            <th>Case Type</th>
                                            <th>Case No</th>
                                            <th>Case Year</th>
                                            <th>Applicant Name</th>
                                            <th>Application Type</th>
                                            <th>Submit Timing</th>
                                            <th>Action</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr  ng-repeat="row in draftList"  class="odd gradeX">
                                             <td>{{row[0]}}</td>
                                             <td>{{row[1]}}</td>
                                             <td>{{row[2]}}</td>
                                             <td>{{row[3]}}</td>  
                                             <td>{{row[4]}}</td>                                        
                                             <td>{{row[5]}}</td>
                                             <td>{{row[6] | date:"dd/MM/yyyy HH:mm:ss"}}</td>
                                              <td>                               
                                             	<button class="btn btn-success" ng-click="previewApplicationDetails(row[7])">Preview</button>
                                             	<button class="btn btn-success" ng-if="row[8]==1000038 || row[8]==1000037" ng-click="updateCaseStatus(row[7],'Approve')">Approve</button>
                                             <button class="btn btn-success" ng-if="row[8]==1000038 || row[8]==1000037" ng-click="updateCaseStatus(row[7],'Reject')">Reject</button>
                                        	</td>
                                          </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>