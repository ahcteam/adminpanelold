<div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>                                
                            </div>
                            <h4 class="panel-title">View Draft and Preview Details</h4>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-striped table-bordered" ng-init="getRejectedCaveats()">
                                    <thead>
                                        <tr>
                                        	<th>Draft No</th>
                                            <th>Diary No</th>
                                            <th>First Petitioner</th>
                                            <th>First Respondent</th>
                                            <th>Case Type</th> 
                                            <th>Submit Timing</th>                                           
                                            <th>Action</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr  ng-repeat="row in draftRejectedList"  class="odd gradeX">
                                            <td>{{row[0]}}</td>
                                             <td>{{row[1]}}</td>
                                             <td>{{row[2]}}</td>
                                             <td>{{row[3]}}</td>                                          
                                             <td>{{row[4]}}</td>
                                             <td>{{row[5] | date:"dd/MM/yyyy HH:mm:ss"}}</td>
                                            <td>                               
                                             	<button class="btn btn-success" ng-click="previewCaveatDetails(row[6])">Preview</button>
                                        	</td>
                                          </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>