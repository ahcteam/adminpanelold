<div class="modal-body">
		<div class="panel-body">
			<div id="report_table" class="table-responsive">
			
                <table width="100%" id="data-table" class="table table-striped table-bordered">
                   <tbody>
                         <tr class="odd gradeX">
                                <td style="width:50%;">Draft Number</td>
                                <td style="width:50%;">{{registerFile.rcd_draft_no}}</td>                                                                        
                         </tr>
                         <tr class="odd gradeX">
                                <td>Diary Number</td> 
                                <td>{{registerFile.rcd_diary_no}}</td>                                                                       
                         </tr>
                   		<tr class="odd gradeX">
                                <td>Case No.</td> 
                                <td>{{srd.registerCase.caseType.ct_label}}/{{srd.registerCase.rcd_case_no}}/{{srd.registerCase.rcd_case_year}}</td>                                                                       
                         </tr>
                   
                         <tr class="odd gradeX">
                                <td>Stamp (Rs.)</td>
                                <td>{{courtFee}}</td>  
                         </tr>
                         <tr class="odd gradeX">
                                <td>Stamp Sufficient</td>
                                <td>{{srd.srd_stamp_suff}}</td>  
                         </tr>
                         <tr class="odd gradeX">
                                <td>Papers Filed</td>
                                <td>{{srd.srd_paper_filed}}</td>  
                         </tr>
                         <tr class="odd gradeX">
                                <td>Intime Upto</td>
                                <td>{{srd.srd_intime | date:"dd/MM/yyyy"}} {{srd.srd_intime_remark}}</td>  
                         </tr>
                         <tr class="odd gradeX">
                                <td>Cognizable By</td>
                                <td>{{srd.srd_cognizable}}</td>  
                         </tr>
                         <tr class="odd gradeX">
                                <td style="vertical-align: top;">Caveat Filed</td>
                                <td>
                                	<table ng-if="caveat.cav_no">
                                		<tbody>
                                			<tr>
                                				 <td>Number: </td>
                               					 <td>{{caveat.cav_no}}/{{caveat.cav_year}}</td>
                                			</tr>
                                			<tr>
                                				 <td>Caveator Name: </td>
                               					 <td>{{caveat.cav_caveator_name}}</td>
                                			</tr>
                                			<!-- <tr>
                                				 <td>Email: </td>
                               					 <td>{{caveat.cav_email}}</td>
                                			</tr>
                                			<tr>
                                				 <td>Mob. No: </td>
                               					 <td>{{caveat.cav_mobile}}</td>
                                			</tr> -->
                                			<tr>
                                				 <td>District: </td>
                               					 <td>{{caveat.district.dt_name}}</td>
                                			</tr>
                                			<!-- <tr>
                                				 <td>Filed by: </td>
                               					 <td>{{caveat.userMaster.um_fullname}} ({{caveat.userMaster.username}})</td>
                                			</tr> -->
                                			
                                		</tbody>
                                	</table>
                                </td>  
                         </tr>
                         <tr class="odd gradeX">
                                <td>Cause of Action Arises At</td>
                                <td ng-if="srd.srd_bench_code==8">Allahabad</td>
                                <td ng-if="srd.srd_bench_code==9">Lucknow</td>  
                         </tr>
                         <tr class="odd gradeX">
                                <td>Remarks, if any</td>
                                <td>{{srd.srd_remark}}</td>  
                         </tr>
                         <tr class="odd gradeX">
                                <td>Category </td>
                                <td>{{srd.srd_category}}</td>  
                         </tr>
                         <tr class="odd gradeX">
                                <td>Date</td>
                                <td>{{srd.srd_cr_date | date:"dd/MM/yyyy"}}</td>  
                         </tr>
                         <tr class="odd gradeX">
                                <td style="vertical-align: top;">Stamp Reporting By</td>
                                <td>Suresh Srivastava, SO <br>{{srd.userMaster.um_fullname}}</td>  
                         </tr>
                    </tbody>
            	</table>
            	
        	</div>
        	<button id="btnPrint" class="btn btn-success">Print</button>
		</div>
</div>
