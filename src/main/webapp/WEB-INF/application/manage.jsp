<jsp:include page="../content/header2.jsp"></jsp:include>  
<div id="content" class="content">
  <div class="container-fluid" ng-controller="ApplicationController" >
  
			<div class="row">
			    <!-- begin col-12 -->
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>                                
                            </div>
                            <h4 class="panel-title">View Application Details</h4>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                            			   <table id="data-table" st-table="displayedCollection"
							st-safe-src="masterdata"
							class="table table-striped table-bordered nowrap table-hover" width="100%">
								<thead>
									<tr>
										<td>
											<select class="form-control" ng-model="search.stageId"
									 			ng-options="stage.lk_id as stage.lk_longname for stage in stages">
												<option value="">Select Stage</option>
											</select>
										</td>
										<td>
											<input type="text" class="form-control" placeholder="Diary No" ng-model="search.diary_no">
										</td>
 										<td>
										  <input type="text" class="form-control" datepicker-popup="{{format}}" name="stageDate" ng-model="search.stageDate" required is-open="stageDate" ng-disabled="true" max-date="maxDate"  datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" show-button-bar="false" />
				            			  <span class="input-group-addon" ng-click="open($event,'stageDate')"><i class="glyphicon glyphicon-calendar"></i></span>
										</td>
										<td>
											<button id="search" type="submit" class="btn btn-primary btn-sm" ng-click="searchApplications()">Search</button>
											<button id="search" type="submit" class="btn btn-primary btn-sm" ng-click="downloadCaseFiles()">Download CSV</button>
											<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#uploadCSV">
												Upload Application CSV
											</button>
										</td>
									</tr>
				 
								</thead>
							</table>
                                <table id="data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                        	<th>Username</th>
                                            <th>Draft No</th>
                                            <th>Diary No</th>
                                            <th>Apl. No</th>
                                            <th>Apl. Year</th>
                                            <th>Case Type</th>
                                            <th>Case No</th>
                                            <th>Case Year</th>
                                            <th>Application Type</th>
                                            <th>Case Status</th>
                                            <th>Action</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in applicationList"  class="odd gradeX">
                                              <td>{{row.userMaster.username}}</td>
                                             <td>{{row.ap_draft_no}}</td>
                                             <td>{{row.ap_diary_no}}</td>   
                                             <td>{{row.ap_no}}</td>
                                             <td>{{row.ap_year}}</td>
                                             <td>{{row.caseFileDetail.caseType.ct_label}}</td>
                                             <td>{{row.caseFileDetail.fd_case_no}}</td>
                                             <td>{{row.caseFileDetail.fd_case_year}}</td>                                    
                                             <td>{{row.applicationType.at_name}}</td>
                                             <td>{{row.caseStage.lk_longname}}</td>
                                             <td>
	                                             <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" ng-click="getStages(row)" data-target="#stageHistory">
													History
												</button>
												<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" ng-click="getReporting(row)" data-target="#reporting">
													Reporting
												</button>
												<button type="button" class="btn btn-primary btn-sm"  ng-click="showDocument(row)">
													View
												</button>
											</td>                                             
                                         </tr>
                                         <tr ng-show="applicationList.length==0">
                                         <td colspan="11"><div class="alert alert-danger">No Records Found</div></td>
                                         </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="uploadCSV" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  		<div class="modal-dialog modal-lg">
							    	<div class="modal-content">
							      	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        	<h4 class="modal-title" id="myModalLabel"><strong>Upload CSV</strong></h4>
							      	</div>	     
						  			<%@ include file="../application/_master_form.jsp"%>
							    	</div>
						  		</div>
							</div>
							<div class="modal fade" id="stageHistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  		<div class="modal-dialog modal-lg">
							    	<div class="modal-content">
							      	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        	<h4 class="modal-title" id="myModalLabel"><strong>Stage History</strong></h4>
							      	</div>	     
						  			<%@ include file="../application/stage_history.jsp"%>
							    	</div>
						  		</div>
							</div>
							<div class="modal fade" id="reporting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  		<div class="modal-dialog modal-lg">
							    	<div class="modal-content">
							      	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        	<h4 class="modal-title" id="myModalLabel"><strong>Reporting History</strong></h4>
							      	</div>	     
						  			<%@ include file="../application/reporting.jsp"%>
							    	</div>
						  		</div>
							</div>
                        </div>
                    </div>
                    <!-- end panel -->
            
                <!-- end col-12 -->
            </div>
           </div>
           </div>
        </div>
            <!-- end row -->
    	</body>
   

	
	
	<!-- ================== END PAGE LEVEL JS ================== -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ng-file-upload.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/angularJs/ngMask.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap/angular-datepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap/ui-bootstrap-tpls.0.11.2.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/scripts/controllers/ApplicationController.js"></script>

	
	<script type="text/javascript"
	 src="${pageContext.request.contextPath}/assets/js/apps.min.js"></script>
	<script>
		$(document).ready(function() {
			App.init();
			
		});
	</script>



</html>