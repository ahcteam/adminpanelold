<jsp:include page="../content/header2.jsp"></jsp:include>
	<div id="content" class="content" ng-controller="searchCaseFileController">
		<div class="row">
			<div class="col-md-12">
				
				<!-- begin panel -->
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<!-- <button type="button" class="btn btn-primary btn-sm pull-right"
							ng-click="resetMasterdata()" data-toggle="modal"
							data-target="#casefile_Modal">
							<span class="glyphicon glyphicon-plus-sign"></span> Add New
							Record
						</button> -->
					</div>
					<h4 class="panel-title">Case File Search</h4>
				</div> 
				<div class="panel-body">
					<div>
						<table id="data-table" st-table="displayedCollection"
							st-safe-src="masterdata"
							class="table table-striped table-bordered nowrap table-hover" width="100%">
								<thead>
									<tr>
										<th style="text-align: center;" width="25%" st-sort="fd_case_year">Case Year</th>
										<th style="text-align: center;" width="40%" st-sort="fd_case_type">Case Type</th>
										<th style="text-align: center;" width="20%"  st-sort="fd_case_no">Case Number</th>
										<th style="text-align: center;" width="10%">Search</th>
									</tr>
									<tr>
										<th>
										 <input id="fd_case_year"  type="text" name="fd_case_year"  class="form-control" ng-pattern="/^[a-zA-Z0-9]*$/"  ng-maxlength=15 ng-model="model.fd_case_year"  />
										</th>
										
										<th>
											<select ng-options="caseType.ct_id as caseType.ct_name for caseType in caseTypeList"
											class="form-control" id="caseTypeData" 
											name="fd_case_type" ng-model="model.fd_case_type" required>
											<option value="">--Select--</option>
											</select>
										</th>
									
										<th>
										 <input id="fd_case_no"  type="text" name="fd_case_no"  class="form-control" ng-pattern="/^[a-zA-Z0-9]*$/"  ng-maxlength=15 ng-model="model.fd_case_no"  />
										</th>
										
														  
 										<th>
										
										<button id="search" type="submit" class="btn btn-primary btn-sm pull-right"
												ng-click="searchCaseFile()" data-toggle="modal" style="size: 2px">
										<span class="glyphicon glyphicon-plus-sign"></span>Search</button>
										</th>
									</tr>
				 
								</thead>
								<tbody>
									<tr ng-repeat="data in caseFileList" class="odd gradeX">
										<td align="center">{{data.fd_case_year}}</td>
										<td align="center">{{data.caseType.ct_name}}</td>
										<td align="center">{{data.fd_case_no}}</td>
										<td align="center">
										<button id="Submit" type="submit" class="btn btn-success"
												ng-click="createApplication(data)" >
										Add Document</button>
										</td>
										
									
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="9" class="text-center">
											<div st-pagination="" st-items-by-page="10"
											st-displayed-pages="8">
											</div>
										</td>
									</tr>
								</tfoot>
						 </table>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-10 -->
						
				</div>
			</div>
		</div>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/scripts/application_controllers/searchCaseFile.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap/ui-bootstrap-tpls.0.11.2.js"></script>


	<!-- ================== END PAGE LEVEL JS ================== -->
	<script src="${pageContext.request.contextPath}/assets/js/apps.min.js"></script>
	<script>
		$(document).ready(function() {
			App.init();
			
		});
	</script>	
	



</html>

