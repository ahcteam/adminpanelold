<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="com.dms.model.ObjectMaster"%>
<%@ page import="com.dms.model.User"%>	
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="EDMSApp">
<head>
<link rel="icon" href="${pageContext.request.contextPath}/images/favicon.ico"/>
<link rel='stylesheet' href='${pageContext.request.contextPath}/css/bootstrap/angular-datepicker.css'>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Allahabad High Court Admin Panel</title>

 <%@ include file="style.jsp"%>
<%@ include file="script.jsp"%>
</head>
<body>
<% 
User user = null;
if(session.getAttribute("USER")!=null)
	 user = (User)session.getAttribute("USER");

String role=user.getUserroles().get(0).getLk().getLk_longname();
System.out.println("Role name="+role);
String url=request.getRequestURL()+"";
String home="";
String casefile="";
String caveat="";
String applications="";
String upload="";
String scrutinycase="";
String scrutinycaveat="";
String scrutinyapplication="";
String olreport="";
String uri=(String) request.getAttribute("javax.servlet.forward.request_uri");

if(uri.equals("/adminpanel/admin/home"))
	home="active";
if(uri.equals("/adminpanel/casefile/manage"))
	casefile="active";
if(uri.equals("/adminpanel/caveat/manage"))
	caveat="active";
if(uri.equals("/adminpanel/application/manage"))
	applications="active";
if(uri.equals("/adminpanel/upload/manage"))
	applications="active";
if(uri.equals("/adminpanel/scrutiny/cases"))
	scrutinycase="active";
if(uri.equals("/adminpanel/scrutiny/caveats"))
	scrutinycaveat="active";
if(uri.equals("/adminpanel/scrutiny/applications"))
	scrutinyapplication="active";
if(uri.equals("/adminpanel/olreport/manage"))
	olreport="active";

%>
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar navbar-default navbar-fixed-top">
			<!-- begin container-fluid -->
			<div class="container-fluid">
				<!-- begin mobile sidebar expand / collapse button -->
				<div class="navbar-header">
					<a href="index.html" class="navbar-brand"><span class="navbar-logo"></span>Allahabad High Court e-Filing</a>
				</div>
				<!-- end mobile sidebar expand / collapse button -->
				
				<!-- begin header navigation right -->
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown navbar-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<img src="${pageContext.request.contextPath}/assets/img/user-13.jpg" alt="" /> 
							<span class="hidden-xs"><%=user.getUm_fullname() %></span> <b class="caret"></b>
						</a>
						<ul class="dropdown-menu animated fadeInLeft">
							<li><a href="${pageContext.request.contextPath}/dms/logout">Log Out</a></li>
							<li><a href="${pageContext.request.contextPath}/user/edit">Edit Profile</a></li>
						</ul>
					</li>
				</ul>
				<!-- end header navigation right -->
			</div>
			<!-- end container-fluid -->
		</div>
		<!-- end #header -->
		
		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				
				<ul class="nav">
				<% if(role.equals("Admin")){ %>
					
					<li class="has-sub <%=home %>">
						<a href="${pageContext.request.contextPath}/admin/home">
						<i class="fa fa-home"></i>
							<span>Home</span>
					    </a>
					</li>
					<li class="has-sub <%=casefile %>">
						<a href="${pageContext.request.contextPath}/casefile/manage">
						<i class="fa fa-file-o"></i>
							<span>Cases</span>
					    </a>
					</li>
					<li class="has-sub <%=caveat %>">
						<a href="${pageContext.request.contextPath}/caveat/manage">
						<i class="fa fa-file-o"></i>
							<span>Caveats</span>
							</a>
					</li>
					<li class="has-sub <%=applications %>">
						<a href="${pageContext.request.contextPath}/application/manage">
						<i class="fa fa-file-o"></i>
							<span>Applications</span>
							</a>
					</li>
					<li class="has-sub <%=olreport %>">
						<a href="${pageContext.request.contextPath}/olreport/manage">
						<i class="fa fa-file-o"></i>
							<span>Office Liquidator Report</span>
					    </a>
					</li>
					<% } %>
					<% if(role.equals("CaseScrutiny") || role.equals("CaseSupervisor")){ %>
					<li class="has-sub <%=scrutinycase %>">
						<a href="${pageContext.request.contextPath}/scrutiny/cases">
						<i class="fa fa-file-o"></i>
							<span>Case</span>
					    </a>
					</li>
					<li class="has-sub <%=scrutinycaveat %>">
						<a href="${pageContext.request.contextPath}/scrutiny/caveats">
						<i class="fa fa-file-o"></i>
							<span>Caveat</span>
							</a>
					</li>
					<% } %>
					<% if(role.equals("ApplicationScrutiny")  || role.equals("ApplicationSupervisor")){ %>
					<li class="has-sub <%=scrutinyapplication %>">
						<a href="${pageContext.request.contextPath}/scrutiny/applications">
						<i class="fa fa-file-o"></i>
							<span>Application</span>
							</a>
					</li>
					<% } %>
					<li class="has-sub ">
						<a href="javascript:;">
						<i class="fa fa-info-circle"></i>
							<span>Help</span>
					    </a>
					</li>					
					<!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		
		<!-- end #sidebar -->
		
		<!-- begin #content -->
			
	<!-- ================== END PAGE LEVEL JS ================== -->

